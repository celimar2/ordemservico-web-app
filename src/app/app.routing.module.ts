import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './core/components/login/login.component';
import {AuthGuard} from "./core/auth/auth.guard";
import {DashComponent} from "./core/components/dash/dash.component";
import {ForgotPasswordComponent} from "./core/components/forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./core/components/reset-password/reset-password.component";

const routes: Routes = [

  // { path: '', component: DashComponent},
  { path: 'reset-password', component: ResetPasswordComponent, canActivate: [AuthGuard] },
  { path: '', component: DashComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent},
  { path: "user", loadChildren: "../app/core/components/user/user.module#UserModule"},
  { path: "role", loadChildren: "../app/core/components/role/role.module#RoleModule"},
  { path: "report", loadChildren: "../app/feature/components/report/report.module#ReportModule"},
  { path: "regiao", loadChildren: "../app/feature/components/regiao/regiao.module#RegiaoModule"},
  { path: "estado", loadChildren: "../app/feature/components/estado/estado.module#EstadoModule"},
  { path: "municipio", loadChildren: "../app/feature/components/municipio/municipio.module#MunicipioModule"},
  { path: "filial", loadChildren: "../app/feature/components/filial/filial.module#FilialModule"},
  { path: "empresa", loadChildren: "../app/feature/components/empresa/empresa.module#EmpresaModule"},
  { path: "fornecedor", loadChildren: "../app/feature/components/fornecedor/fornecedor.module#FornecedorModule"},
  { path: "colaborador", loadChildren: "../app/feature/components/colaborador/colaborador.module#ColaboradorModule"},
  { path: "marca-veiculo", loadChildren: "../app/feature/components/marca-veiculo/marca-veiculo.module#MarcaVeiculoModule"},
  { path: "veiculo", loadChildren: "../app/feature/components/veiculo/veiculo.module#VeiculoModule"},
  { path: "atividade-economica", loadChildren: "../app/feature/components/atividade-economica/atividade-economica.module#AtividadeEconomicaModule"},
  { path: "ordem-servico", loadChildren: "../app/feature/components/ordem-servico/ordem-servico.module#OrdemServicoModule"},
  { path: "natureza-servico", loadChildren: "../app/feature/components/natureza-servico/natureza-servico.module#NaturezaServicoModule"},
  { path: "situacao-servico", loadChildren: "../app/feature/components/situacao-servico/situacao-servico.module#SituacaoServicoModule"},
  // { path: '**', redirectTo: 'home' },
  // { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
  // { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
  // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [],
  declarations: []
})
export class AppRoutingModule { }
