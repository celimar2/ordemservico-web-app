import {Page} from "./page.model";

export interface RolePage {
  id: number;
  page: Page;
  crud: string;
  // create: boolean;
  // read: boolean;
  // update: boolean;
  // delete: boolean;
  // execute: boolean;
}
