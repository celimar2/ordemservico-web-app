export interface MenuTree{
  id: number;
  name: string;
  sequence: number;
  ativo: boolean;
  nodes: MenuTree[];
}
