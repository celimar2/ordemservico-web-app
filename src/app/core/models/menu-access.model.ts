export interface MenuAccess {
  id: number;
  name: string;
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}
