export interface Menu {
  id: number;
  name: string;
  description: string;
  sequence: number;
  ativo: boolean;
  // page: Page;
  crud: string;
  parentMenuId: number;
  parentMenuName: string;
}
