
export interface Parametro {
  chave: string;
  valor: string;
  observacao: string;
}
