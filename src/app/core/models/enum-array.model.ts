
export interface EnumArrayModel {
  key: string;
  value: string;
  idx: number;
}
