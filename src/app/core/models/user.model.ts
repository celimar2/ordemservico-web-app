import {Role} from "./role.model";

export interface User {

  id: number;
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  active: boolean;
  blocked: boolean;
  changePasswordNextLogin: boolean;
  passwordAltered: Date;
  lastLogin: Date;
  loginAttempts: number;
  roles: Role[];
}
