import {Menu} from "./menu.model";

export interface RoleMenu {
  id: number;
  menu: Menu;
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}
