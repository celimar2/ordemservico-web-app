import {RoleMenu} from "./role-menu.model";

export interface Role {

  id: number;
  name: string;
  description: string;
  active: boolean;
  roleMenuList: RoleMenu[];
}
