import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user.model';
import {Globals} from "../../shared/globals";
import {Role} from "../models/role.model";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private userUrl = this.globals.HOSTNAME_PORT + '/api/users';


  public getList() {
    return this.http.get<User[]>(this.userUrl);
  }

  public getListNoColaborador() {
    return this.http.get<User[]>(this.userUrl+"/no-colaborador");
  }

  public getById(id) {
    return this.http.get<User>(this.userUrl + "/"+id);
  }

  public getByColaborador(id) {
    return this.http.get<User>(this.userUrl + "/colaborador/"+id);
  }

  public getRolesById(id) {
    return this.http.get<Role[]>(this.userUrl + "/"+id+"/roles");
  }

  public update(user) {
    return this.http.put(this.userUrl, user);
  }

  public create(user) {
    return this.http.post<User>(this.userUrl, user);
  }

  public delete(id) {
    return this.http.delete(this.userUrl + "/"+ id);
  }



}
