import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from "../../shared/globals";
import {EnumModel} from "../models/enum.model";


@Injectable({
  providedIn: 'root'
})
export class EnumsService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private userUrl = this.globals.HOSTNAME_PORT + '/api/enums';


  public getTipoTelefone() {
    return this.http.get<EnumModel[]>(this.userUrl + "/tipos-telefone");
  }

  public getTipoJuridico() {
    return this.http.get<EnumModel[]>(this.userUrl + "/tipos-juridico");
  }

  public getGenero() {
    return this.http.get<EnumModel[]>(this.userUrl + "/generos");
  }

  public getSituacaoVeiculo() {
    return this.http.get<EnumModel[]>(this.userUrl + "/situacoes-veiculos");
  }

  // public  enumToArray(someEnum): {key: string; value: string}[] {
  public  enumToArray(someEnum): any[] {
    const mapAux: {key: string; value: string; idx: number}[] = [];
    let i = 0 ;
    mapAux.push({key: '', value: 'Selecione', idx: 0 });
    for(let n in someEnum) {
      i = i+ 1;
      mapAux.push({key: n, value: <any>someEnum[n], idx: i });
    }
    return mapAux;
  }

}
