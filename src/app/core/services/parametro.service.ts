import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Parametro} from '../models/parametro.model';
import {Globals} from "../../shared/globals";

@Injectable({
  providedIn: 'root'
})
export class ParametroService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private userUrl = this.globals.HOSTNAME_PORT + '/api/parametros';


  public getList() {
    return this.http.get<Parametro[]>(this.userUrl);
  }

  public getById(id) {
    return this.http.get<Parametro>(this.userUrl + "/"+id);
  }

  public update(parametro) {
    return this.http.put(this.userUrl, parametro);
  }

  public create(parametro) {
    return this.http.post<Parametro>(this.userUrl, parametro);
  }

  public delete(id) {
    return this.http.delete(this.userUrl + "/"+ id);
  }

}
