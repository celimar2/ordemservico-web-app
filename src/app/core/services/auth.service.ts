import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Globals} from "../../shared/globals";
import {TokenStorage} from "../auth/token.storage";
import {Observable} from "rxjs/Observable";
import {RoleMenu} from "../models/role-menu.model";
import {MenuAccess} from "../models/menu-access.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private router: Router,
              private globals: Globals,
              public token: TokenStorage) {
  }

  private URL_AUTH = this.globals.HOSTNAME_PORT + '/api/authenticate';
  private URL_AUTH_ACCESS = this.globals.HOSTNAME_PORT + '/api/users/access/';
  private URL_RESET_PWD = this.globals.HOSTNAME_PORT + '/api/reset-password';

  private forgotPassword: boolean = false;
  private resetPassword: boolean = false;

  public setUserAccess(username: string){
    this.http.get<RoleMenu[]>(this.URL_AUTH_ACCESS+username).subscribe(
      res => {
        this.token.setAccess(res);
      }
    )
  }

  public getMenuAccess(menuName: string): MenuAccess{
    return this.token.getMenuAccess(menuName);
  }

  public authenticate(user: string, pwd: string): Observable<any> {
    this.token.setUsername(user);
    this.token.clearToken();
    const credentials = {username: user, password: pwd};
    return this.http.post<any>(this.URL_AUTH, credentials)
  }


  public isLogged(): boolean {
    return this.token.hasToken();
  }

  public getUsername(): string {
    return this.token.getUsername();
  }

  public logout() {
    this.router.navigate(['/']);
    this.token.clearToken();
    this.token.clearUser();
  }

  public setForgotPassword(value) {
    this.forgotPassword = value;
  }

  public setResetPassword(value) {
    this.forgotPassword = value;
  }

  public passwordForgotten() {
    return this.forgotPassword;
  }

  public isResetPassword() {
    return this.resetPassword;
  }

  sendPasswordEmail(email: string): any {
    return this.http.post<any>(this.URL_RESET_PWD, email)
      // .pipe(map((res: any) => {
      //   // login successful if there's a jwt token in the response
      //   if (res && res.message) {
      //     alert(res.message);
      //   }
      // }));
      // .switchMap(res => alert(res))
      // .subscribe(
      //   data => alert('data: '+data),
      //   error => alert('error: '+error)
      // );
      // .pipe(map((res:any) => {
      //   // alert(JSON.stringify(res));
      //   if (res && res.token) {
      //     this.token.saveToken(res.token);
      //     console.log('login res.token' + res.token)
      //   }
      // }));
  }
}
