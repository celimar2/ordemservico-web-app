import {Injectable} from '@angular/core';
import {Menu} from "../models/menu.model";
import {MenuTree} from "../models/menu-tree.model";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public convertMenuToTree( menu: Menu[] ): MenuTree {
    let root: MenuTree;
    menu.forEach(m => {
      if (m.parentMenuId == null) {
        root = this.menuToTree(m);
      }
    })
    this.getTreeNode(menu, root);
    return root;
  }

  private getTreeNode(menu: Menu[], menuTree: MenuTree) {
    let nodes = this.getNodes(menu, menuTree.id);
    nodes.forEach(ch => {
      let branch = this.menuToTree(ch);
      this.getTreeNode(menu, branch);
      menuTree.nodes.push(branch);
    });
  }

  private getNodes(menu: Menu[], id: number): Menu[] {
    let nodes: Menu[] = [];
    menu.forEach(m => {
      if (m.parentMenuId == id)
        nodes.push(m);
    })
    return nodes;
  }

  private menuToTree(menu): MenuTree {
    let menuTree: MenuTree = {id: menu.id, name: menu.name, sequence: menu.sequence, ativo: menu.ativo, nodes: [] }
    return menuTree;
  }
}
