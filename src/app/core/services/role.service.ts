import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {Role} from "../models/role.model";
import {Page} from "../models/page.model";
import {Menu} from "../models/menu.model";
import {RoleMenu} from "../models/role-menu.model";

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient, private globals: Globals) {
  }

  private roleUrl = this.globals.HOSTNAME_PORT + '/api/roles';


  public getList() {
    return this.http.get<Role[]>(this.roleUrl);
  }

  public getNewPageList() {
    return this.http.get<Page[]>(this.roleUrl + "/pages");
  }

  public getNewMenuList() {
    return this.http.get<Menu[]>(this.roleUrl + "/menus");
  }

  public getPagesById(id) {
    return this.http.get<Page>(this.roleUrl + "/" + id + "/pages");
  }

  public getMenusById(id) {
    return this.http.get<RoleMenu[]>(this.roleUrl + "/" + id + "/menus");
  }

  public getListAtivos() {
    return this.http.get<Role[]>(this.roleUrl + "/ativos");
  }

  public getById(id) {
    return this.http.get<Role>(this.roleUrl + "/" + id);
  }
  public getByName(name) {
    return this.http.get<Role>(this.roleUrl + "/name/"+name);
  }


  public update(role) {
    return this.http.put(this.roleUrl, role);
  }

  public create(role) {
    return this.http.post<Role>(this.roleUrl, role);
  }

  public delete(id) {
    return this.http.delete(this.roleUrl + "/" + id);
  }

}

