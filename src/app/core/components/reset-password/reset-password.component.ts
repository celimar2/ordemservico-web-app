import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material";
import {AuthService} from "../../services/auth.service";
import {User} from "../../models/user.model";
import {ErrorDialogComponent} from "../../../shared/dialogs/error-dialog.component";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {}

  public isLoading: boolean = false;
  detailForm: FormGroup;
  userEmail: string;
  error = '';
  hide = true;

  sub: any;
  mode: string;
  userId: string;
  token: string;
  user: User;
  msg: any;


  ngOnInit() {
    this.authService.setResetPassword(true);

    this.sub = this.route.queryParams
      .subscribe(params => {
        // this.mode = params['mode'] || "";
        this.userId = params['id'] || "0";
        this.userEmail = params['mail'] || "";
        this.token = params['token'] || "";
      });

    // this.authService.validateToken()

    this.detailForm = this.fb.group({
      password: ['', Validators.required],
      confirmPass: ['', Validators.required],
    });

  }

  get f() { return this.detailForm.controls; }

  onSubmit() {
    if (this.detailForm.invalid) {
      return;
    }

    if (this.detailForm.get('password').value != this.detailForm.get('password').value ) {
      alert("Erro \n As duas senhas devem ser iguais!");
      return;
    }


    this.isLoading = true;
    this.authService.authenticate(this.f.username.value, this.f.password.value)
    // .pipe(first())
      .subscribe(
        data => {
          this.isLoading = false;
          // alert("data: \n"+JSON.stringify(data));
          if (data.token) {
            this.authService.token.saveToken(data.token);
            this.router.navigate(['/login']);
          } else {
            this.showError("Erro", "Credenciais inválidas!", JSON.stringify(data));
            // convenience getter for easy access to form fields
          }
        },
        err => {
          this.isLoading = false;
          // alert("error: \n"+JSON.stringify(err))
          this.error = err;
          this.showError("Erro", "Credenciais inválidas!", JSON.stringify(err));
        });
  }

  showError(title: string, message : string, error: any= null) : void {
    this.dialog.open(ErrorDialogComponent,
      {data: {title: title, message: message, error: error}, width : 'max-content'}
    );
  }

  cancel() {
  // forgotPassword() {
    this.isLoading = false;
    this.authService.setResetPassword(false);
    this.router.navigate(['/login']);
  }

}
