import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  constructor( private formBuilder: FormBuilder,
               private authService: AuthService,
               private route: ActivatedRoute,
               private router: Router,) { }

  public isLoading: boolean = false;
  frm: FormGroup;

  ngOnInit() {
    this.frm = this.formBuilder.group({
      email: ['', Validators.email]
    });
  }

  onSubmit() {
    this.isLoading = true;
    this.authService.sendPasswordEmail(this.frm.controls.email.value)
      .subscribe(
        data => {
          this.isLoading = false;
          alert(data.message);
        },
        error => {
          this.isLoading = false;
          alert('error: '+error)
        }
      );
  }

  cancel() {
    this.authService.setForgotPassword(false);
  }

}
