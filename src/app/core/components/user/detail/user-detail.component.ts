import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../../models/user.model";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Role} from "../../../models/role.model";
import {RoleService} from "../../../services/role.service";
import {RoleSelectDialogComponent} from "../../dialogs/role-select-dialog.component";
import {MatDialog} from "@angular/material";
import {MenuAccess} from "../../../models/menu-access.model";
import {AuthService} from "../../../services/auth.service";


@Component({
  selector: 'user-edit',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
// export class UserDetailComponent implements OnInit, AfterViewChecked {
  // @ViewChild("username") nameField: ElementRef  ;
  // ngAfterViewChecked() {
  //   this.nameField.nativeElement.focus();
  // }
  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private router: Router,
              private auth: AuthService,
              private route: ActivatedRoute,
              private userService: UserService,
              private roleService: RoleService) {
  }

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  userId: string;
  user: User;
  detailForm: FormGroup;
  allRoles: Role[];
  msg: any;

  // newRole: Role;

  ngOnInit() {

    this.sub = this.route.queryParams
      .subscribe(params => {
        this.mode = params['mode'] || "";
        this.userId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("Usuario");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.detailForm = this.fb.group({
      id: 0,
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPass: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      active: [false, Validators.required],
      blocked: [false, Validators.required],
      changePasswordNextLogin: [false, Validators.required],
      passwordAltered: null,
      lastLogin: null,
      loginAttempts: '0',
      roles: this.fb.array([this.initRoles()])
    }, {validator: this.checkIfMatchingPasswords('password', 'confirmPass')});

    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }
    if (this.mode != 'insert') {
      this.userService.getById(this.userId).subscribe(resp => {
          const roles = this.detailForm.get('roles') as FormArray;
          while (roles.length) {
            roles.removeAt(0);
          }
          this.detailForm.patchValue(resp);
          resp.roles.forEach(role => {
              roles.push(this.fb.group(role))
          });
          this.user = resp;
          this.detailForm.get('confirmPass').setValue(this.user.password);

        },
        error => this.msg = <any>error)
    };

    this.roleService.getListAtivos().subscribe(resp => {
      this.allRoles = resp;
    });

  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  // ----------------------------------------------------------------------------------------
  //roles
  // ----------------------------------------------------------------------------------------
  initRoles() {

    this.roleService.getByName("USER").subscribe(
      resp => {
        let role: Role = resp;
        return this.fb.group({
          id: role.id,
          name: role.name,
          description: role.description,
          active: role.active,
          roleMenuList: [],
          // roleMenuList: role.roleMenuList,
        });

      },
      error => {
        this.msg = <any>error;
        return this.fb.group({});
      })
  }

    addRolesItem(role: Role) {
    return this.fb.group({
      id: role.id,
      name: role.name,
      description: role.description,
      active: role.active,
      roleMenuList: [],
      // roleMenuList: role.roleMenuList,
    })
  }

  removeRole(idx: number) {
    const rolesArray = <FormArray>this.detailForm.controls['roles'];
    rolesArray.removeAt(idx);
  }

  addRole(): void {
    this.openDialog();
  }

  openDialog() {
    const userRoles = this.detailForm.get('roles').value;
    const diffRoles = this.allRoles.filter(function (obj) {
      return !userRoles.some(function (obj2) {
        return obj.id == obj2.id;
      });
    });

    const dialogRef = this.dialog.open(RoleSelectDialogComponent, {
      width: '350px', //height: '300px',
      data: diffRoles
    });

    dialogRef.afterClosed().subscribe(result => {
      const rolesArray = <FormArray>this.detailForm.controls['roles'];
      const newRole = this.addRolesItem(result.value);
      rolesArray.push(newRole);
    });
  }

  validData(): boolean {

    if (this.detailForm.get('password').value != this.detailForm.get('confirmPass').value) {
      alert('A senha não confere!')
      return false;
    }
    const rolesArray = <FormArray>this.detailForm.controls['roles'];
    rolesArray.controls.forEach( role  => role.value.roleMenuList = []);
    return true;
  }

  onSubmit() {
    if (!this.validData()) {
      return
    }
    if (this.mode == 'insert') {
      this.userService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['user']);
          },
          error => {
            alert('Erro=>: ' + error);
          });
    } else if (this.mode == 'update') {
      this.userService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['user']);
          },
          error => {
            alert('Erro==>: ' + error);
          });
    }
  }

}
