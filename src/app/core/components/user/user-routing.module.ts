import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./list/user.component";
import {UserDetailComponent} from "../user/detail/user-detail.component";


const routes: Routes = [
  { path: "", component: UserComponent},
  { path: "detail", component: UserDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class UserRoutingModule { }
