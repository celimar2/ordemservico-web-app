import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {User} from '../../../models/user.model';
import {Router} from '@angular/router';
import {UserService} from "../../../services/user.service";
import {ConfirmDialogComponent} from '../../../../shared/dialogs/confirm-dialog.component';
import {AuthService} from "../../../services/auth.service";
import {MenuAccess} from "../../../models/menu-access.model";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService,
              private auth: AuthService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  users: User[];
  displayedColumns = [ 'username', 'firstName', 'lastName', 'active', 'blocked', 'actionsColumn'];
  dataSource = new MatTableDataSource<User>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.userService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
   filterValue = filterValue.trim(); // Remove whitespace
   filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
   this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Usuario");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.userService.getList()
      .subscribe( data => {
        this.users = data;
      });
  }

  delete(user: User){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userService.delete(user.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == user.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
        })
      }
    });

  };

  show(user: User): void {
    if (this.accessRead) {
      this.router.navigate(['user/detail'], {queryParams: {mode: "read", id: user.id.toString()}});
      this.loadData();
    }
  };

  edit(user: User): void {
    if (this.accessUpdate) {
      this.router.navigate(['user/detail'], {queryParams: {mode: "update", id: user.id.toString()}});
      this.loadData();
    }
  };

  add(): void {
    this.router.navigate(['user/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}

