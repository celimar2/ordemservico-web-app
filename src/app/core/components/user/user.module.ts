import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from "../user/user-routing.module";
import {UserComponent} from "./list/user.component";
import {UserDetailComponent} from "../user/detail/user-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {RoleSelectDialogComponent} from "../dialogs/role-select-dialog.component";
import {RoleService} from "../../services/role.service";

@NgModule({
  imports: [CommonModule, UserRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule ],
  declarations: [UserComponent, UserDetailComponent],
  providers: [UserService ,
    RoleService,
    RoleSelectDialogComponent
  ]
})
export class UserModule { }
