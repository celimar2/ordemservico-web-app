import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OsListByFornecedor} from "../../../feature/models/ordem-servico-list.model";


@Component({
  selector: 'os-dialog',
  templateUrl: './os-dialog.component.html'
})
export class OsDialogComponent{

  dialogForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<OsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OsListByFornecedor[]) {
  }

  ngOnInit() {
      this.dialogForm = this.fb.group({})
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // submit(form) {
  //   this.dialogRef.close(`${role}`);
  // }

  // confirm(role) {
  //   this.dialogRef.close(`${role}`);
  // }

  // submit(form) {
  //   this.dialogRef.close(`${true}`);
  // }

  }
