import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Role} from "../../models/role.model";


@Component({
  selector: 'role-select-dialog',
  templateUrl: './role-select-dialog.component.html'
})
export class RoleSelectDialogComponent{

  dialogForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<RoleSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Role[]) {
  }

  ngOnInit() {
      this.dialogForm = this.fb.group({})
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // submit(form) {
  //   this.dialogRef.close(`${role}`);
  // }

  confirm(role) {
    this.dialogRef.close(`${role}`);
  }

  // submit(form) {
  //   this.dialogRef.close(`${true}`);
  // }

  }
