import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResetPasswordComponent} from "../reset-password/reset-password.component";
import {ForgotPasswordComponent} from "../forgot-password/forgot-password.component";

const routes: Routes = [
  { path: "reset-password", component: ResetPasswordComponent},
  { path: "forgot-password", component: ForgotPasswordComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilRoutingModule { }
