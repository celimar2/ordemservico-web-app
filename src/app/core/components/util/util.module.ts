import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UtilRoutingModule} from './util-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RoleService} from "../../services/role.service";
import {UserService} from "../../services/user.service";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {RoleSelectDialogComponent} from "../dialogs/role-select-dialog.component";
// import {ResetPasswordComponent} from "../reset-password/reset-password.component";
// import {ForgotPasswordComponent} from "../forgot-password/forgot-password.component";

@NgModule({
  imports: [CommonModule, UtilRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule ],
  declarations: [],
  // declarations: [ResetPasswordComponent, ForgotPasswordComponent],
  providers: [UserService , RoleService, RoleSelectDialogComponent]
})
export class UtilModule { }


