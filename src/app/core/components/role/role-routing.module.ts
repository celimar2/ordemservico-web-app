import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {RoleComponent} from "./list/role.component";
import {RoleDetailComponent} from "../role/detail/role-detail.component";


const routes: Routes = [
  { path: "", component: RoleComponent},
  { path: "detail", component: RoleDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class RoleRoutingModule { }
