import {Component, OnInit, ViewChild} from '@angular/core';
import {Role} from "../../../models/role.model";
import {Router} from "@angular/router";
import {RoleService} from "../../../services/role.service";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../services/auth.service";
import {MenuAccess} from "../../../models/menu-access.model";

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  constructor(private router: Router,
              private roleService: RoleService,
              private auth: AuthService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  roles: Role[];
  displayedColumns = [ 'id', 'name', 'description', 'active', 'actionsColumn'];
  dataSource = new MatTableDataSource<Role>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.roleService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("PerfilAcesso");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.roleService.getList()
      .subscribe( data => {
        this.roles = data;
      });
  }

  delete(role: Role){
    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.roleService.delete(role.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == role.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(role: Role): void {
    if (this.accessRead) {
      this.router.navigate(['role/detail'],{ queryParams: { mode:"read", id:role.id.toString()} });
    }
  };

  edit(role: Role): void {
    if (this.accessUpdate) {
      this.router.navigate(['role/detail'],{ queryParams: { mode:"update", id:role.id.toString()} });
    }
  };

  add(): void {
    this.router.navigate(['role/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


