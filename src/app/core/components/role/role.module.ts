import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoleRoutingModule} from "../role/role-routing.module";
import {RoleComponent} from "./list/role.component";
import {RoleDetailComponent} from "../role/detail/role-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RoleService} from "../../services/role.service";
import { FilterRolePipe } from './filter-role.pipe';

@NgModule({
  imports: [CommonModule, RoleRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [RoleComponent, RoleDetailComponent, FilterRolePipe],
  providers: [ RoleService ]
})
export class RoleModule { }
