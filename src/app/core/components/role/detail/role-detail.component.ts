import {Component, OnInit} from "@angular/core";
import {Role} from "../../../models/role.model";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RoleService} from "../../../services/role.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";
import {Menu} from "../../../models/menu.model";
import {MenuService} from "../../../services/menu.service";
import {RoleMenu} from "../../../models/role-menu.model";
import {MenuAccess} from "../../../models/menu-access.model";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.scss']
})
export class RoleDetailComponent implements OnInit {
// export class RoleDetailComponent implements OnInit, AfterViewChecked {
//   @ViewChild("name") nameField: ElementRef ;

  constructor(private roleService: RoleService,
              private fb: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private menuService: MenuService,
              private route: ActivatedRoute) {
  }

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  roleId: string;
  role: Role;
  roleMenus: RoleMenu[] = [];
  menus: Menu[] = [];
  // menuTree: MenuTree;
  menuIndex: {id:number,ident:number}[] = [];

  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.roleId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("PerfilAcesso");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.detailForm = this.fb.group({
      id: [0, {disabled: true}],
      name: ['', Validators.required],
      description: ['', Validators.required],
      active: [false],
      // roleMenuList: this.fb.array([]),
      roleMenuList: this.fb.array([this.initRoleMenu()]),
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode == 'insert') {
      this.loadNewMenu();
    }else {

      this.roleService.getById(this.roleId).subscribe(
        data => {
          // console.log(JSON.stringify(data));
          // this.detailForm.setValue(data)
          const roleMenu = this.detailForm.get('roleMenuList') as FormArray;
          while (roleMenu.length) {
            roleMenu.removeAt(0);
          }
          this.detailForm.patchValue(data);
          data.roleMenuList.forEach(itm => {
            this.roleMenus.push(itm);
            this.menus.push(itm.menu);
            roleMenu.push(this.fb.group(itm))
          });
          if (this.roleMenus.length == 0) {
            this.loadNewMenu();
          }
          this.calculateLevel(this.roleMenus);
      })


    }
  }


  loadNewMenu() {
    this.roleService.getNewMenuList().subscribe(
      data => {
        this.loadMenuTable(data);
        this.roleMenus.forEach(rm => {
          this.menus.push(rm.menu);
          this.addRoleMenu(rm);
        });
        this.calculateLevel(this.roleMenus);
      })
  }

  getArray(menu): any {
    let arr: number[] = [];
    arr.length = this.getLevel(menu);
    return arr;
  }

  isBranch(menu: Menu): boolean {
    let res = false;
    this.menuIndex.forEach( i => {
      if (menu.id === i.id)
        res = true;
    })
    return res;
  }

  getLevel(menu:Menu): number {
    let res = 0;
    this.menuIndex.forEach( i => {
      if (menu.id === i.id)
        res = i.ident
    })
    return res;
  }

  calculateLevel(roleMenu: RoleMenu[]) {
    let level = 0;
    roleMenu.forEach( m => {
      level = 0;
      this.menuIndex.forEach( i => {
        if (m.menu.parentMenuId === i.id)
          level = i.ident+1
      })
      this.menuIndex.push({id: m.menu.id, ident: level})
    })
  }

  menuCrud(menu: Menu, crud:string): boolean {
    let result = false;
    if (menu != null && menu != undefined) {
      if (menu.crud != null && menu.crud != undefined) {
        result = menu.crud.toUpperCase().indexOf(crud.toUpperCase()) > -1;
      }
    }
    // if (menu != null && menu != undefined) {
    //    if (menu.page != null && menu.page != undefined) {
    //     if (menu.page.crud != null && menu.page.crud != undefined) {
    //       result = menu.page.crud.toUpperCase().indexOf(crud.toUpperCase()) > -1;
    //     }
    //   }
    // }
    return result;
  }

  initRoleMenu() {
    return this.fb.group({
      id: 0,
      menu: this.fb.array([this.initMenu()]),
      create: false,
      read: false,
      update: false,
      delete: false,
    });
  }

  initMenu() {
    return this.fb.group({
      id: 0,
      name: '',
      description: '',
      sequence: 0,
      ativo: false,
      page: this.fb.array([this.initPage()]),
      parentMenuId: '',
      parentMenuName: '',
    });
  }

  initPage() {
    return this.fb.group({
      id: 0,
      title: '',
      url: '',
      crud: '',
    });
  }

  addRoleMenuItem(menu: Menu) {
    return this.fb.group({
      id: 0,
      menu: menu,
      create: false,
      read: false,
      update: false,
      delete: false,
    });
  }

  addRoleMenu(rm: RoleMenu) {
    const rolesMenus = <FormArray>this.detailForm.controls['roleMenuList'];
    const newRoleMenu = this.addRoleMenuItem(rm.menu);
    rolesMenus.push(newRoleMenu);
  }

  // ngAfterViewChecked() {
  //   this.nameField.nativeElement.focus();
  // }
  selectAll(select:boolean) {
    const rolesMenus = <FormArray>this.detailForm.controls['roleMenuList'];
    rolesMenus.controls.forEach( rm => {
      let roleMenu = rm;
      roleMenu.value.create = select && this.menuCrud(roleMenu.value.menu,'C');
      roleMenu.value.read =  select && (this.menuCrud(roleMenu.value.menu,'R')  || this.isBranch(roleMenu.value.menu));
      roleMenu.value.update = select && this.menuCrud(roleMenu.value.menu,'U');
      roleMenu.value.delete = select && this.menuCrud(roleMenu.value.menu,'D');
      rm.setValue(roleMenu.value);
    })
  }

  isAllSelected(selected:boolean) {
    const rolesMenus = <FormArray>this.detailForm.controls['roleMenuList'];
    let qtd = 0;
    let sel = 0;
    rolesMenus.controls.forEach( roleMenu => {
      qtd = qtd + (this.menuCrud(roleMenu.value.menu,'C') ? 1 :0);
      qtd = qtd + ((this.menuCrud(roleMenu.value.menu,'R') || this.isBranch(roleMenu.value.menu)) ? 1 :0);
      qtd = qtd + (this.menuCrud(roleMenu.value.menu,'U') ? 1 :0);
      qtd = qtd + (this.menuCrud(roleMenu.value.menu,'D') ? 1 :0);

      sel = sel + (roleMenu.value.create  ? 1 : 0 ) ;
      sel = sel + (roleMenu.value.read ? 1 : 0 ) ;
      sel = sel + (roleMenu.value.update  ? 1 : 0 ) ;
      sel = sel + (roleMenu.value.delete  ? 1 : 0 ) ;
    })

    if (selected) {
      return sel == qtd;
    } else {
      return sel == 0;
    }
  }

  loadMenuTable(menuList: Menu[]) {

    this.roleMenus = [];
    this.menuIndex = [];
    // let page: Page = null;
    let menu: Menu = null;
    let rm: RoleMenu = null;

    menuList.forEach(m => {
      // page = null;
      // if (m.page != null) {
      //   page = {
      //     id: m.page.id,
      //     url: m.page.url,
      //     title: m.page.title,
      //     crud: m.page.crud,
      //   };
      // } else {
      //   page = null;
      // }

      menu = {
        id: m.id,
        description: m.description,
        name: m.name,
        sequence: m.sequence,
        ativo: m.ativo,
        // page: page,
        crud: m.crud,
        parentMenuId: m.parentMenuId,
        parentMenuName: m.parentMenuName,
      };

      rm = {
        id: 0,
        menu: menu,
        create: this.menuCrud(m,'C'),
        read:   this.menuCrud(m,'R'),
        update: this.menuCrud(m,'U'),
        delete: this.menuCrud(m,'D'),
      }
      this.roleMenus.push(rm);
      this.menuIndex.push({id:m.id, ident:0});
    })

    // this.menuTree = this.menuService.convertMenuToTree(this.menu);

      // this.roleService.getNewMenuList().subscribe(
      //   data => {
      //     this.rolePages = [];
      //     this.menu = [];
      //     let page: Page = null;
      //     let rp: RolePage = null;
      //     data.forEach(m => {
      //       page = null;
      //       if (m.page != null) {
      //         page = { id: m.id, url: m.page.url, title: m.page.title, crud: m.page.crud };
      //       }
      //       let rp: RolePage = {id: m.id, name: m.name , page: page, crud: m.crud}
      //       this.rolePages.push(rp);
      //       this.menu.push(m)
      //     })
      //     this.menuTree = this.menuService.convertMenuToTree(this.menu);
      //   }
      // );
  }

  // onCancel() {
  //   this.router.navigate(['role']);
  //   return;
  // }

  onSubmit() {
    if (this.mode == "insert") {
      this.roleService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['role']);
            return;
          },
          error => {
            alert('Erro: ' + error);
          });

    } else if (this.mode == "update") {
      this.roleService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['role']);
            return;
          },
          error => {
            alert('Erro: ' + error);
          });
    }
  }

}
