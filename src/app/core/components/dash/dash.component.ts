import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material";
import {AuthService} from "../../services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrdemServicoService} from "../../../feature/services/ordem-servico.service";
import {OsListByFornecedor} from "../../../feature/models/ordem-servico-list.model";
import {TinyOrdemServico} from "../../../feature/models/tiny-ordem-servico.model";
import * as moment from "moment";
//import {OsDialogComponent} from "../dialogs/os-dialog.component";
import {now} from "moment";


@Component({
  selector: 'dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {

  constructor(private router: Router,
              public dialog: MatDialog,
              public  auth: AuthService,
              private _formBuilder: FormBuilder,
              private ordemServicoService: OrdemServicoService,
              private osService: OrdemServicoService,
              )
  {}

  // public roleMenu: RoleMenu[] = [];

  public osItemList: OsListByFornecedor = null;
  public osList: OsListByFornecedor[] = [];
  public rowCount: number = 0;
  loading: boolean = false;
  osPendente: boolean = true;
  placa: string = '';
  fornecedor: string = '';
  

  isLinear = false;
  cadastroVeiculoFg: FormGroup;
  cadastroEntradaOsFg: FormGroup;
  cadastroSaidaOsFg: FormGroup;

  ngOnInit() {
    // this.cadastroVeiculoFg = this._formBuilder.group({
    //   firstCtrl: ['', Validators.required]
    // });
    // this.cadastroEntradaOsFg = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required]
    // });
    // this.cadastroSaidaOsFg = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required]
    // });

      // this.roleMenu = JSON.parse(window.sessionStorage.getItem("roleMenu"));

    this.loadData();
  }

  loadData() {
    this.rowCount = 0;
    this.loading = true;
    this.osList = [];
    this.osService.getListByFornecedor().subscribe(
      data => {
        if (data != null) {
          data.forEach(item => {
            this.osList.push(item);
            // this.osItemList = item;
            if (item.ordens.length > this.rowCount) {
              this.rowCount = item.ordens.length;
              // if (this.osItemList.ordens.length > this.rowCount) {
              //   this.rowCount = this.osItemList.ordens.length ;
            }
          });
        }
        this.osList.sort((a: OsListByFornecedor, b: OsListByFornecedor) => a.apelido > b.apelido ? 1 : a.apelido < b.apelido ? -1 : 0);
        this.loading = false;
      });
  }

  counter(i: number) {
    return new Array(i);
  }

  getOs(forn: OsListByFornecedor, row: number, info: string) {
    if (row < forn.ordens.length) {
      let os: TinyOrdemServico = forn.ordens.slice(row)[0];
      if (info == 'placa') {
        return os == undefined ? "" : os.placa;
      } else if (info == 'id') {
        return os == undefined ? 0 : os.id;
      } else if (info == 'os') {
        return os;
      } else if (info == 'delayed') {
        return moment().isAfter(os.previsao, "day") ? "delayed" : "ontrack";
      } else if (info == 'info') {
        return os == undefined ? "" :
          os.marca + " - " + os.modelo + ' ' + os.cor +
          "\n Natureza: " + os.natureza +
          "\n Serviço : " + os.descricao +
          "\n Situação: " + os.situacao +
          "\n Abertura: " + moment(os.abertura).format("DD/MM/YYYY") +
          (os.previsao != null ? "\n Previsão: " + moment(os.previsao).format("DD/MM/YYYY") : "") +
          (moment().isAfter(os.previsao, "day") ? "\n atrazado: " + moment().diff(os.previsao, "days") +" dia(s)":"")
          ;
      } else
        return os;
    }
  }

  openDetail(forn: OsListByFornecedor, row: number) {
    // const f: OsListByFornecedor = forn;
    // // f.ordens = [];
    // // f.ordens.push(this.getOs(forn,row,'os') );

    // const dialogRef = this.dialog.open(OsDialogComponent, {
    //   width: '350px', //height: '300px',
    //   data: f
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   alert(JSON.stringify(result));
    //   // const rolesArray = <FormArray>this.detailForm.controls['roles'];
    //   // const newRole = this.addRolesItem(result.value);
    //   // rolesArray.push(newRole);
    // });
    let id = this.getOs(forn, row, 'id');
    this.router.navigate(['ordem-servico/detail'], {queryParams: {mode: "update", id: id.toString()}});
  }

  // ---------------------------------------------------------------------

  // cards = [
  //   { id:1, title: 'Card 1', cols: 2, rows: 1},
  //   { id:2, title: 'Card 2', cols: 1, rows: 1 },
  //   { id:3, title: 'Card 3', cols: 1, rows: 2 },
  //   { id:4, title: 'Card 4', cols: 1, rows: 1 }
  // ];
  //
  //
  // // -------------------------------------------------------------------------------------------------------------
  // public barChartOptions:any = {
  //   scaleShowVerticalLines: false,
  //   responsive: true
  // };
  // public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  // public barChartType:string = 'bar';
  // public barChartLegend:boolean = true;
  //
  // public barChartData:any[] = [
  //   {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
  //   {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  // ];
  //
  // // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }
  //
  // public chartHovered(e:any):void {
  //   console.log(e);
  // }
  //
  // public randomize():void {
  //   // Only Change 3 values
  //   let data = [
  //     Math.round(Math.random() * 100),
  //     59,
  //     80,
  //     (Math.random() * 100),
  //     56,
  //     (Math.random() * 100),
  //     40];
  //   let clone = JSON.parse(JSON.stringify(this.barChartData));
  //   clone[0].data = data;
  //   this.barChartData = clone;
  //   /**
  //    * (My guess), for Angular to recognize the change in the dataset
  //    * it has to change the dataset variable directly,
  //    * so one way around it, is to clone the data, change it and then
  //    * assign it;
  //    */
  // }
  //
  //
  // //------------------------------------------------------------------------------------------------------------------
  // // lineChart
  // public lineChartData2:Array<any> = [
  //   {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
  //   {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
  //   {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
  // ];
  // public lineChartLabels2:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  // public lineChartOptions2:any = {
  //   responsive: true
  // };
  // public lineChartColors2:Array<any> = [
  //   { // grey
  //     backgroundColor: 'rgba(148,159,177,0.2)',
  //     borderColor: 'rgba(148,159,177,1)',
  //     pointBackgroundColor: 'rgba(148,159,177,1)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  //   },
  //   { // dark grey
  //     backgroundColor: 'rgba(77,83,96,0.2)',
  //     borderColor: 'rgba(77,83,96,1)',
  //     pointBackgroundColor: 'rgba(77,83,96,1)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(77,83,96,1)'
  //   },
  //   { // grey
  //     backgroundColor: 'rgba(148,159,177,0.2)',
  //     borderColor: 'rgba(148,159,177,1)',
  //     pointBackgroundColor: 'rgba(148,159,177,1)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  //   }
  // ];
  // public lineChartLegend2:boolean = true;
  // public lineChartType2:string = 'line';
  //
  // public randomize2():void {
  //   let _lineChartData:Array<any> = new Array(this.lineChartData2.length);
  //   for (let i = 0; i < this.lineChartData2.length; i++) {
  //     _lineChartData[i] = {data: new Array(this.lineChartData2[i].data.length), label: this.lineChartData2[i].label};
  //     for (let j = 0; j < this.lineChartData2[i].data.length; j++) {
  //       _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
  //     }
  //   }
  //   this.lineChartData2 = _lineChartData;
  // }
  // public chartClicked2(e:any):void {
  //   console.log(e);
  // }
  //
  // public chartHovered2(e:any):void {
  //   console.log(e);
  // }
  //
  // //------------------------------------------------------------------------------------------------------------------
  //
  // // lineChart
  // public lineChartData:Array<any> = [
  //   [65, 59, 80, 81, 56, 55, 40],
  //   [28, 48, 40, 19, 86, 27, 90]
  // ];
  // public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  // public lineChartType:string = 'line';
  // public pieChartType:string = 'pie';
  //
  // // Pie
  // public pieChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
  // public pieChartData:number[] = [300, 500, 100];
  //
  //
  // public randomizeType():void {
  //   this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
  //   this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  // }
  //
  // public chartClicked3(e:any):void {
  //   console.log(e);
  // }
  //
  // public chartHovered3(e:any):void {
  //   console.log(e);
  // }
  //
  // //------------------------------------------------------------------------------------------------------------------


}
