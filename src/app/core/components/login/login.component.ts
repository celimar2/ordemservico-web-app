import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ErrorDialogComponent} from '../../../shared/dialogs/error-dialog.component';
import {MatDialog} from "@angular/material";
import {AuthService} from "../../services/auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {}

  public isLoading: boolean = false;
  loginForm: FormGroup;
  returnUrl: string;
  error = '';
  hide = true;

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // reset login status
    this.authService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.authService.authenticate(this.f.username.value, this.f.password.value)
      // .pipe(first())
      .subscribe(
        data => {
          this.isLoading = false;
          if (data.token) {
            this.authService.token.saveToken(data.token);
            this.router.navigate([this.returnUrl]);
            this.authService.setUserAccess(this.f.username.value);
          } else {
            this.showError("Erro", "Credenciais inválidas!", JSON.stringify(data));
          }
        },
        err => {
          this.isLoading = false;
          // alert("error: \n"+JSON.stringify(err))
          this.error = err;
          this.showError("Erro", "Credenciais inválidas!", JSON.stringify(err));
        });
  }

  showError(title: string, message : string, error: any= null) : void {
    this.dialog.open(ErrorDialogComponent,
      {data: {title: title, message: message, error: error}, width : 'max-content'}
    );
  }

  forgotPassword() {
    this.isLoading = false;
    this.authService.setForgotPassword(true);
    // this.router.navigate(['/forgot-password']);
  }

}
