import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EnumToArrayPipe} from './enum-to-array.pipe';
import { FilterOsPipe } from '../filter-os.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EnumToArrayPipe, FilterOsPipe],
  exports: [EnumToArrayPipe, FilterOsPipe]
})
export class PipesCommonModule { }


