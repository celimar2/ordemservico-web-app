import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TokenStorage} from "./token.storage";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private tokenStorage: TokenStorage) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    // let currentUser = JSON.parse(this.tokenStorage.getUsername());
    if (this.tokenStorage.hasToken()) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.tokenStorage.getToken()}`
        }
      });
    }

    return next.handle(request);
  }
}
