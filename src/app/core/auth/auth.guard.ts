import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private auth: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if ( this.auth.isLogged() ) {
      return true;
    }

    // not logged in so redirect to login page with the return url
    if ( (this.router.url != '/login') && (this.router.url != '/forgot-password') && (!this.router.url.includes('/reset-password?')) ) {
      // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    }
    return false;
  }
}
