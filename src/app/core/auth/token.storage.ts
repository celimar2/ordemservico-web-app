import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {RoleMenu} from "../models/role-menu.model";
import {Menu} from "../models/menu.model";
import {MenuAccess} from "../models/menu-access.model";

@Injectable()
export class TokenStorage {

  constructor(private globals: Globals) {
  }

  public setUsername(username: string) {
    window.sessionStorage.setItem(this.globals.USERNAME, username);
  }

  public getUsername(): string {
    return window.sessionStorage.getItem(this.globals.USERNAME);
  }

  public clearUser() {
    window.sessionStorage.removeItem(this.globals.USERNAME);
    window.sessionStorage.removeItem(this.globals.USERACCESS);
  }

  public setAccess(roleMenu: RoleMenu[]) {
    // let menuAccess: MenuAccess[] = []
    // // console.log("user access: ========================================================== \n\n"+JSON.stringify(roleMenu));
    // roleMenu.forEach(value => {
    //   value.menu.crud = String(value.menu.crud).toUpperCase();
    //   // console.log(' ==================================== ');
    //   // console.log("menu: "+value.menu.name +", crud: "+value.menu.crud +" > "+
    //   //   (value.create ? 'C' : ' ') +
    //   //   (value.read ? 'R' : ' ')  +
    //   //   (value.update ? 'U' : ' ')  +
    //   //   (value.delete ? 'D' : ' ')
    //     // ',c-'+value.menu.crud.indexOf('C') +
    //     // ',r-'+value.menu.crud.indexOf('R')+
    //     // ',u-'+value.menu.crud.indexOf('U')+
    //     // ',d-'+value.menu.crud.indexOf('D') +
    //     // ((value.create && value.menu.crud.indexOf('C') > -1) ? 'C' : ' ' )+
    //     // ((value.read   && value.menu.crud.indexOf('R') > -1 ) ? 'R': ' ')  +
    //     // ((value.update && value.menu.crud.indexOf('U') > -1) ? 'U' : ' ' ) +
    //     // ((value.delete && value.menu.crud.indexOf('D') > -1) ? 'D' : ' ')
    //   // );
    //   menuAccess.push({
    //     id: value.menu.id,
    //     name: value.menu.name,
    //     crud: ((value.create && value.menu.crud.indexOf('C') > -1) ? 'C': ' ') +
    //       ((value.read && value.menu.crud.indexOf('R') > -1) ? 'R': ' ')  +
    //       ((value.update && value.menu.crud.indexOf('U') > -1) ? 'U': ' ')  +
    //       ((value.delete && value.menu.crud.indexOf('D') > -1) ? 'D': ' ') ,
    //     parentMenuId: value.menu.parentMenuId,
    //   })
    // })
    // // console.log("============================================================================================================");
    // // console.log("MENU ACCESS -------------------------------------------\n: "+JSON.stringify(menuAccess));
    // // console.log("============================================================================================================");
    // window.sessionStorage.removeItem(this.globals.USERACCESS);
    // window.sessionStorage.setItem(this.globals.USERACCESS, JSON.stringify(menuAccess));
    window.sessionStorage.setItem(this.globals.USERACCESS, JSON.stringify(roleMenu));
  }

  // public getAccess(): MenuAccess[] {
  public getAccess(): RoleMenu[] {
    return JSON.parse(window.sessionStorage.getItem(this.globals.USERACCESS));
  }

  public getItemAccess(item: string, crud: string): boolean {
    let roleMenu: MenuAccess = this.getMenuAccess(item);
    return (crud == 'C' && roleMenu.create) ||
      (crud == 'R' && roleMenu.read) ||
      (crud == 'U' && roleMenu.update) ||
      (crud == 'D' && roleMenu.delete);
  }

  public getMenuAccess(item: string): MenuAccess {
    let result: MenuAccess = {id: 0, name: "", create: false, read: false, update: false, delete: false};
    // console.log("getMenuAccess(item: string):  " + item);
    let roleMenuList: RoleMenu[] = this.getAccess();
    if (roleMenuList != null && roleMenuList != undefined) {
      let roleMenu: RoleMenu = roleMenuList.filter(x => x.menu.name == item)[0];
      if (roleMenu != null && roleMenu != undefined) {
        if (roleMenu.menu != null && roleMenu.menu != undefined) {
          // console.log("roleMenu: " + roleMenu);
          result.id = roleMenu.menu.id;
          result.name = roleMenu.menu.name;
          result.create =roleMenu.create;
          result.read = roleMenu.read;
          result.update = roleMenu.update;
          result.delete = roleMenu.delete;
          return result;
        }
      }
    }
    // let result: MenuAccess = {id: 0, name: "", create: true, read: true, update: true, delete: true};
    return result;
  }

  public clearToken() {
    window.sessionStorage.removeItem(this.globals.TOKEN_KEY);
  }

  public saveToken(token: string) {
    this.clearToken();
    window.sessionStorage.setItem(this.globals.TOKEN_KEY, token);
    // public saveToken(token: string, username: string) {
    // window.sessionStorage.removeItem(USERNAME);
    // window.sessionStorage.setItem(USERNAME,  username);
  }

  public getToken(): string {
    return window.sessionStorage.getItem(this.globals.TOKEN_KEY);
  }

  public hasToken(): boolean {
    return (this.getToken() != null);
  }

}

