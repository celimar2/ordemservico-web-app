import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { UserProfileComponent } from './components/user-profile/detail/user-profile.component';

@NgModule({
  imports: [CommonModule],
  declarations: [UserProfileComponent]
})
export class CoreModule { }
