import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CustomGuiModule} from './shared/custom-gui.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";

import {FooterComponent} from './core/components/footer/footer.component';
import {LoginComponent} from './core/components/login/login.component';
import {AlertComponent} from './core/components/alert/alert.component';
import {AlertService} from "./core/services/alert.service";
import {Globals} from "./shared/globals";
import {AuthGuard} from "./core/auth/auth.guard";
import {LayoutModule} from '@angular/cdk/layout';
import {DashComponent} from './core/components/dash/dash.component';
import {NavComponent} from "./core/components/nav/nav.component";
import {ErrorDialogComponent} from './shared/dialogs/error-dialog.component';
import {ConfirmDialogComponent} from './shared/dialogs/confirm-dialog.component';
import {RoleSelectDialogComponent} from "./core/components/dialogs/role-select-dialog.component";
import {PipesCommonModule} from "./core/pipes/pipes-common/pipes-common.module";
import {AuthService} from "./core/services/auth.service";
import {TokenStorage} from "./core/auth/token.storage";
import {ErrorInterceptor} from "./core/auth/error.interceptor";
import {JwtInterceptor} from "./core/auth/jwt.inteceptor";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialogModule} from "@angular/material";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {ChartsModule} from 'ng2-charts';
import {registerLocaleData} from "@angular/common";
import 'moment/locale/br';
import localePt from '@angular/common/locales/pt';
import {AtividadeEconomicaSelectDialogComponent} from "./feature/components/fornecedor/cadastrar/atividade-economica-select-dialog.component";
import {PlacaVeiculoDialogComponent} from "./feature/components/placa-veiculo-dialog/placa-veiculo-dialog.component";
import {UserSelectDialogComponent} from "./feature/components/colaborador/user-select-dialog.component";
import {ForgotPasswordComponent} from "./core/components/forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./core/components/reset-password/reset-password.component";
import {OsDialogComponent} from "./core/components/dialogs/os-dialog.component";
// import { CapitalizePipe } from './core/pipes/pipes-common/capitalize-pipe';
// import { LocalizedDatePipe } from './core/pipes/pipes-common/localized-date-pipe';
import { LocalCurrencyPipe } from './shared/pipes/local-currency.pipe';
// import { DateTimePipe } from './core/pipes/pipes-common/date-time.pipe';
// import {GlobalErrorHandler} from "./core/services/GlobalErrorHandler";

registerLocaleData(localePt);

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    LayoutModule,
    PipesCommonModule,
    CustomGuiModule,
    ChartsModule,
    MatDialogModule,
  ],
  declarations: [
    AppComponent,
    NavComponent,
    DashComponent,
    FooterComponent,
    LocalCurrencyPipe,
    //
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    //
    AlertComponent,
    ErrorDialogComponent,
    ConfirmDialogComponent,
    RoleSelectDialogComponent,
    OsDialogComponent,
    AtividadeEconomicaSelectDialogComponent,
    UserSelectDialogComponent,
    PlacaVeiculoDialogComponent,
  ],
  entryComponents: [
    ErrorDialogComponent,
    ConfirmDialogComponent,
    RoleSelectDialogComponent,
    OsDialogComponent,
    AtividadeEconomicaSelectDialogComponent,
    UserSelectDialogComponent,
    PlacaVeiculoDialogComponent,
  ],
  providers: [
    // DatePipe,
    // DateTimePipe,
    // CapitalizePipe,
    // LocalizedDatePipe,
    // LocalCurrencyPipe,
    // ErrorDialogComponent,
    // ConfirmDialogComponent,
    AlertService,
    Globals,
    AuthService,
    AuthGuard,
    TokenStorage,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    // { provide: LOCALE_ID, useValue: 'pt-BR'},
    { provide: LOCALE_ID, useFactory: () => 'pt-BR'},
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
    { provide: MAT_DATE_FORMATS, useValue: 'pt-BR'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    // { provide: ErrorHandler, useClass: GlobalErrorHandler},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
