import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatSidenavModule, MatStepperModule,
  MatTabsModule,
  MatToolbarModule
} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';

import {TextMaskModule} from "angular2-text-mask";
import {NgxCurrencyModule} from "ngx-currency";


const guiModules = [
  TextMaskModule, NgxCurrencyModule];

const materialModules = [MatStepperModule, MatSlideToggleModule,
  MatProgressSpinnerModule, MatRadioModule, MatGridListModule, MatChipsModule,
  MatProgressBarModule, MatPaginatorModule, MatExpansionModule, MatFormFieldModule, MatMomentDateModule,
  MatAutocompleteModule, MatTooltipModule,
  // MatNativeDateModule,
  MatCheckboxModule, MatDatepickerModule, MatTabsModule, MatSidenavModule, MatListModule, MatMenuModule, MatIconModule, CommonModule,
  MatToolbarModule, MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatTableModule, MatSelectModule];

@NgModule({
  imports: [materialModules, guiModules],
  exports: [materialModules, guiModules],
})
export class CustomGuiModule { }
