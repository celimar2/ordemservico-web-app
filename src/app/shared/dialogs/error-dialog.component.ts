import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'error-dialog',
  templateUrl: './error-dialog.component.html'
})

export class ErrorDialogComponent {

  // public dialog: MatDialog;

  constructor(private dialogRef: MatDialogRef<ErrorDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  public closeDialog() {
    this.dialogRef.close();
  }

  // public showError(title: string, message : string) : void {
  //   this.dialog.open(ErrorDialogComponent,
  //     {data: {title: title, message: message}, width : '350px'}
  //   );
  // };


}
