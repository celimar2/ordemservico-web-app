import {Injectable} from "@angular/core";


@Injectable()
export class Globals {

  public TOKEN_KEY: string = 'AuthToken';
  public TOKEN_HEADER_KEY: string = 'Authorization'
  public USERNAME: string = 'Username';
  public USERACCESS: string = 'UserAccess';
  //////////////////////////////////////////////////////
  public HOSTNAME: string = 'https://gradual.net.br';
  public PORT: string = '8443';
  public CONTEXT: string = '/servicos';
  public LOGO_IMAGE = "./assets/img/logo.png";
  ////////////////////////////////////////////////
  // public HOSTNAME: string = 'http://localhost';
  // public PORT: string = '8080';
  // public CONTEXT: string = '';
  // public LOGO_IMAGE = '';
  //////////////////////////////////////////////////////
  public HOSTNAME_PORT: string = this.HOSTNAME + ':'+ this.PORT + this.CONTEXT;
  public LOCALE_BR: string = 'pt-BR';
  public DATE_FMT: string = "YYYY-MM-DD";
  public DATETIME_FMT: string = "YYYY-MM-DDThh:mm:ss";

// /^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/;

  public ANO_VALIDATOR = /^(\d{4}|)$/;
  public PLACA_PREFIXO_VALIDATOR = /^[a-zA-Z]{3}$/;
  public PLACA_SUFIXO_VALIDATOR = /^\d{4}$/;
  public PLACA_VALIDATOR = /^([A-Z]{3}\-\d{4}|)$/;
  public TEL_VALIDATOR = /^(\(\d{2}\)\d{4}\-\d{4}|)$/;
  public CEP_VALIDATOR = /^(\d{2}\.\d{3}\-\d{3}|)$/;
  public CPF_VALIDATOR = /^(\d{3}\.\d{3}\.\d{3}\-\d{2}|)$/;
  public CNPJ_VALIDATOR = /^(\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}|)$/;
  public CPF_CNPJ_VALIDATOR = /^(\d{3}\.\d{3}\.\d{3}\-\d{2}|\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}|)$/;

  public ANO_MASK: any[] = [/\d/,/\d/,/\d/,/\d/];
  public PLACA_MASK: any[] = [/[A-Z]/,/[A-Z]/,/[A-Z]/,'-',/\d/,/\d/,/\d/,/\d/];
  public CPF_MASK: any[] = [/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'-',/\d/,/\d/];
  public CNPJ_MASK: any[] = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public CEP_MASK: any[] = [/\d/,/\d/,'.',/\d/,/\d/,/\d/,'-',/\d/,/\d/,/\d/];
  public TEL_MASK: any[] = ['(',/\d/,/\d/,')',/\d/,/\d/,/\d/,/\d/,'-',/\d/,/\d/,/\d/,/\d/];

};


