import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CpfCnpjValidatorDirective} from './validator/cpf-cnpj-validator.directive';
import {NumbersOnlyDirective} from './validator/numbers-only.directive';
import {AutofocusDirective} from './autofocus.directive';

@NgModule({
  imports: [CommonModule,],
  declarations: [CpfCnpjValidatorDirective, NumbersOnlyDirective, AutofocusDirective],
  exports: [CpfCnpjValidatorDirective, NumbersOnlyDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
