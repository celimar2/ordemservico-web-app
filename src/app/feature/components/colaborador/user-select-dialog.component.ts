import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from "../../../core/models/user.model";

@Component({
  selector: 'user-select-dialog',
  templateUrl: './user-select-dialog.component.html'
})
export class UserSelectDialogComponent{

  dialogForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UserSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User[]) {
  }

  ngOnInit() {
      this.dialogForm = this.fb.group({
        user: [null, Validators.required]
      })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  select() {
    this.dialogRef.close( this.dialogForm.get('user').value );
  }

  // confirm(user) {
  //   alert(JSON.stringify(user));
  //   this.dialogRef.close(`${user}`);
  // }

  // submit(form) {
  //   this.dialogRef.close(`${true}`);
  // }

  }
