import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Municipio} from "../../../models/municipio.model";
import {ColaboradorService} from "../../../services/colaborador.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Colaborador} from "../../../models/colaborador.model";
import {EstadoService} from "../../../services/estado.service";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {first} from "rxjs/operators";
import {Estado} from "../../../models/estado.model";
import {Globals} from "../../../../shared/globals";
import {User} from "../../../../core/models/user.model";
import {UserService} from "../../../../core/services/user.service";
import * as moment from "moment";
import {MatDialog} from "@angular/material";
import {UserSelectDialogComponent} from "../user-select-dialog.component";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-colaborador-detail',
  templateUrl: './colaborador-detail.component.html',
  styleUrls: ['./colaborador-detail.component.scss']
})
export class ColaboradorDetailComponent implements OnInit {

  // @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;

  constructor(private colaboradorService: ColaboradorService,
              private estadoService: EstadoService,
              private userService: UserService,
              private enumService: EnumsService,
              private globals: Globals,
              private fb: FormBuilder,
              private auth: AuthService,
              public dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute) {
  }

  isValidMoment: boolean = false;

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  loading: boolean = false;
  mode: string;
  colaboradorId: string;
  colaborador: Colaborador = null;
  detailForm: FormGroup;
  estados: Estado[] = [];
  municipios: Municipio[] = [];
  msg: any;
  cpfNew: string = "";
  userAux: User = null;

  public generos: any[];
  public tiposTelefone: any[];
  public cpfMask: any = this.globals.CPF_MASK;
  public cepMask: any;
  public telMask: any;

  ngOnInit() {

    this.cpfMask = this.globals.CPF_MASK;
    this.cepMask = this.globals.CEP_MASK;
    this.telMask = this.globals.TEL_MASK;

    this.sub = this.route.queryParams
      .subscribe(
        params => {
          this.mode = params['mode'] || "";
          this.colaboradorId = params['id'] || "0";
          this.cpfNew = params['cpf'] || "";
        });
    this.menuAccess = this.auth.getMenuAccess("Colaborador");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;


    this.initForm();

    if (this.mode != 'insert') {
      this.colaboradorService.getById(this.colaboradorId)
        .subscribe(resp => {
            const telefones = this.detailForm.get('telefones') as FormArray;
            while (telefones.length) {
              telefones.removeAt(0);
            }
            this.detailForm.patchValue(resp);
            // add form array values in a loop
            resp.telefones.forEach(tel =>
              telefones.push(this.fb.group(tel))
            );
            this.colaborador = resp;
            this.loadAuxData();
            this.selectMunicipioPorEstado(this.colaborador.estado);

            if (resp.user != null) {
              this.userAux = resp.user ;
            }
          },
          error => this.msg = <any>error)
    } else {
      this.loadAuxData();
    }
  }

  loadAuxData() {
    this.loading = true;

    this.estadoService.getList().subscribe(
      data => {this.estados = data;},
      () => {this.loading = false;}
    );

    this.loading = true;
    this.enumService.getGenero().subscribe(
      data => {
        this.generos = this.enumService.enumToArray(data);},
      () => {this.loading = false;}
    );

    this.loading = true;
    this.enumService.getTipoTelefone().subscribe(
      data => {this.tiposTelefone = this.enumService.enumToArray(data);},
      () => {this.loading = false;}
    );
  }

  initForm() {
    this.detailForm = this.fb.group({
      id: 0,
      nome: ['', Validators.required],
      cpf: [this.cpfNew, [Validators.required, Validators.pattern(this.globals.CPF_VALIDATOR)]],
      apelido: ['', Validators.required],
      identidadeNumero: ['', Validators.required],
      identidadeOrgaoExpedidor: ['', Validators.required],
      genero: '',
      nascimento: null,
      email: ['', Validators.email],
      ativo: true,
      user: null,
      notas: '',
      cep: ['', Validators.pattern(this.globals.CEP_VALIDATOR)],
      logradouro: ['', Validators.required],
      complemento: '',
      bairro: '',
      estado: [null, Validators.required],
      municipio: [null, Validators.required],
      telefones: this.fb.array([]),
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

  }

  // ---------------------------------------------------------------------------
  // telefones
  // ---------------------------------------------------------------------------

  initFormTelefone() {
    return this.fb.group({
      tipo: ['', Validators.required],
      numero: ['', [Validators.required, Validators.pattern(this.globals.TEL_VALIDATOR)]],
    });
  }

  selectMunicipioPorEstado(estado) {
    this.loading = true;
    this.estadoService.getMunicipioByUf(estado.uf).subscribe(
      data => {this.municipios = data;},
      () => {this.loading = false;}
    );
  }

  removeTelefone(idx: number) {
    const telefoneList = <FormArray>this.detailForm.controls['telefones'];
    telefoneList.removeAt(idx);
  }

  addTelefone(): void {
    const telefoneList = <FormArray>this.detailForm.controls['telefones'];
    const newTelefone = this.initFormTelefone();
    telefoneList.push(newTelefone);
  }

  // ---------------------------------------------------------------------------

  getUserName(): string {
    return this.detailForm.get('user').value != null
      ? this.detailForm.get('user').value.username
      : "";
  }

  clearUser() {
    this.detailForm.get('user').setValue(null);
  }
  findUser(): void  {
    let userList: User[] =[];
    this.userService.getListNoColaborador().subscribe(
      data => {
        data.forEach(
          usr => {userList.push( usr )}
        );
      },
      () => {
        this.loading = false;
      }
    );

    const dialogRef = this.dialog.open(UserSelectDialogComponent,
      {width: '350px', data: userList}
    );

    dialogRef.afterClosed().subscribe(
      result => {
        this.userAux = result;
        if ( this.userAux != null) {
          this.detailForm.get('user').setValue(this.userAux);
        }
      }
    );

  }

  onSubmit() {
    this.detailForm.value.nascimento = moment(this.detailForm.value.nascimento).format("YYYY-MM-DD");

    if (this.mode == "insert") {

      this.colaboradorService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['colaborador']);
            return;
          },
          error => {
            alert(error.message);
          });

    } else if (this.mode == "update") {
      this.colaboradorService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['colaborador']);
            return;
          },
          error => {
            alert(error.message);
          });
    }
  }

  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

}
