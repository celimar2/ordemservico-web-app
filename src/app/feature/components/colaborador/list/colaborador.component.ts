import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ColaboradorService} from "../../../services/colaborador.service";
import {Router} from "@angular/router";
import {Colaborador} from "../../../models/colaborador.model";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-colaborador',
  templateUrl: './colaborador.component.html',
  styleUrls: ['./colaborador.component.scss']
})
export class ColaboradorComponent implements OnInit {


  constructor(private router: Router,
              private auth: AuthService,
              private colaboradorService: ColaboradorService,
              public dialog: MatDialog) {}

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  colaboradores: Colaborador[];
  displayedColumns = ['id', 'nome', 'apelido', 'ativo', 'actionsColumn'];
  dataSource = new MatTableDataSource<Colaborador>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.colaboradorService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Colaborador");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.colaboradorService.getList()
      .subscribe( data => {
        this.colaboradores = data;
      });
  }

  delete(colaborador: Colaborador){
    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.colaboradorService.delete(colaborador.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex(x => x.id == colaborador.id);
            this.dataSource.data.splice(idx, 1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });
  };

  show(colaborador: Colaborador): void {
    if (this.accessRead) {
      this.router.navigate(['colaborador/detail'], {queryParams: {mode: "read", id: colaborador.id.toString()}});
      this.loadData();
    }
  }

  edit(colaborador: Colaborador): void {
    if (this.accessUpdate) {
      this.router.navigate(['colaborador/detail'], {queryParams: {mode: "update", id: colaborador.id.toString()}});
      this.loadData();
    }
  };

  add(): void {
    this.router.navigate(['colaborador/cadastrar']);
    this.loadData();
  };

}

