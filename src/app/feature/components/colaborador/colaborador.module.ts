import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {ColaboradorComponent} from "./list/colaborador.component";
import {ColaboradorDetailComponent} from "./detail/colaborador-detail.component";
import {ColaboradorService} from "../../services/colaborador.service";
import {ColaboradorRoutingModule} from "./colaborador-routing.module";
import {ColaboradorCadastrarComponent} from "./cadastrar/colaborador-cadastrar.component";

@NgModule({

  imports: [CommonModule, ColaboradorRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [ColaboradorComponent, ColaboradorDetailComponent, ColaboradorCadastrarComponent],
  providers: [ ColaboradorService ]
})
export class ColaboradorModule { }
