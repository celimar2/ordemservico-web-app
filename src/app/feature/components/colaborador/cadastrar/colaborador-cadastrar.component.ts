import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ColaboradorService} from "../../../services/colaborador.service";
import {Router} from "@angular/router";
import {Colaborador} from "../../../models/colaborador.model";
import {Globals} from "../../../../shared/globals";

@Component({
  selector: 'app-colaborador-cadastrar',
  templateUrl: './colaborador-cadastrar.component.html',
  styleUrls: ['./colaborador-cadastrar.component.scss']
})
export class ColaboradorCadastrarComponent implements OnInit {


  constructor(private router: Router,
              private fb: FormBuilder,
              private colaboradorService:  ColaboradorService,
              private globals: Globals) {}

  public detailForm: FormGroup;
  public colaboradorResult: Colaborador = null;
  public cpfMask: any;
  public cpfValidator: any;

  ngOnInit() {

    this.cpfMask = this.globals.CPF_MASK;
    this.cpfValidator = this.globals.CPF_VALIDATOR;

    this.detailForm = this.fb.group({
      cpf: ['', [Validators.required, Validators.pattern(this.cpfValidator)]],
    })
  }

  cancel(): void {
    this.router.navigate(['colaborador']);
  }

  submit(form) {
    const cpfRequest = {value: this.detailForm.get('cpf').value};
    this.colaboradorService.getByCpf(cpfRequest)
      .subscribe(
        data => {
          this.colaboradorResult = data;
          if (this.colaboradorResult == null) {
            this.router.navigate(['colaborador/detail'],
              {queryParams: {mode: "insert", id: null, cpf: this.detailForm.get('cpf').value}});
          }
        },
        error => {
          alert(error);
          // let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        }
      )
  }

}

