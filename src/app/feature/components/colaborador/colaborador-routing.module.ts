import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ColaboradorComponent} from './list/colaborador.component';
import {ColaboradorDetailComponent} from './detail/colaborador-detail.component';
import {ColaboradorCadastrarComponent} from "./cadastrar/colaborador-cadastrar.component";

const routes: Routes = [
  { path: "", component: ColaboradorComponent},
  { path: "detail", component: ColaboradorDetailComponent},
  { path: "cadastrar", component: ColaboradorCadastrarComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class ColaboradorRoutingModule { }
