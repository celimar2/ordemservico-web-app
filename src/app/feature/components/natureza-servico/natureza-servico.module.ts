import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NaturezaServicoComponent} from "./list/natureza-servico.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NaturezaServicoService} from "../../services/natureza-servico.service";
import {NaturezaServicoDetailComponent} from "./detail/natureza-servico-detail.component";
import {NaturezaServicoRoutingModule} from "./natureza-servico-routing.module";

@NgModule({
  imports: [CommonModule, NaturezaServicoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [NaturezaServicoComponent, NaturezaServicoDetailComponent],
  providers: [ NaturezaServicoService ]
})
export class NaturezaServicoModule { }
