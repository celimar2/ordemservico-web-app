import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NaturezaServico} from "../../../models/natureza-servico.model";
import {NaturezaServicoService} from "../../../services/natureza-servico.service";
import {first} from "rxjs/operators";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-natureza-servico-detail',
  templateUrl: './natureza-servico-detail.component.html',
  styleUrls: ['./natureza-servico-detail.component.scss']
})
export class NaturezaServicoDetailComponent implements OnInit {

  constructor(private naturezaServicoService: NaturezaServicoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  naturezaServicoId: string;
  naturezaServico: NaturezaServico;
  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.naturezaServicoId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("NaturezaServico");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.detailForm = this.formBuilder.group({
      id: [null,{disabled: true}],
      nome: ['', Validators.required],
      observacao: [''],
      ativo: [false, Validators.required]
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode != 'insert') {
      this.naturezaServicoService.getById(this.naturezaServicoId)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }


  // onCancel() {
  //   this.router.navigate(['naturezaServico']);
  //   return;
  // }

  onSubmit() {
    if (this.mode == "insert") {
      this.naturezaServicoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['natureza-servico']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.naturezaServicoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['natureza-servico']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}


