import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NaturezaServicoDetailComponent} from './natureza-servico-detail.component';

describe('NaturezaServicoDetailComponent', () => {
  let component: NaturezaServicoDetailComponent;
  let fixture: ComponentFixture<NaturezaServicoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaturezaServicoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaturezaServicoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
