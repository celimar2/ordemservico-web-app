import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {NaturezaServicoComponent} from "./list/natureza-servico.component";
import {NaturezaServicoDetailComponent} from "../natureza-servico/detail/natureza-servico-detail.component";


const routes: Routes = [
  { path: "", component: NaturezaServicoComponent},
  { path: "detail", component: NaturezaServicoDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class NaturezaServicoRoutingModule { }
