import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {NaturezaServico} from "../../../models/natureza-servico.model";
import {Router} from "@angular/router";
import {NaturezaServicoService} from "../../../services/natureza-servico.service";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-natureza-servico',
  templateUrl: './natureza-servico.component.html',
  styleUrls: ['./natureza-servico.component.scss']
})
export class NaturezaServicoComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private naturezaServicoService: NaturezaServicoService,
              public dialog: MatDialog) {
  }
  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  naturezaServicos: NaturezaServico[];

  displayedColumns = [ 'id', 'nome', 'observacao', 'ativo', 'actionsColumn'];

  dataSource = new MatTableDataSource<NaturezaServico>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.naturezaServicoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("NaturezaServico");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.naturezaServicoService.getList()
      .subscribe( data => {
        this.naturezaServicos = data;
      });
  }

  delete(naturezaServico: NaturezaServico){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.naturezaServicoService.delete(naturezaServico.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == naturezaServico.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(naturezaServico: NaturezaServico): void {
    if (this.accessRead) {
      this.router.navigate(['natureza-servico/detail'],{ queryParams: { mode:"read", id:naturezaServico.id.toString()} });
    }
  };

  edit(naturezaServico: NaturezaServico): void {
    if (this.accessUpdate) {
      this.router.navigate(['natureza-servico/detail'],{ queryParams: { mode:"update", id:naturezaServico.id.toString()} });
    }
  };

  add(): void {
    this.router.navigate(['natureza-servico/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


