import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NaturezaServicoComponent} from './natureza-servico.component';

describe('NaturezaServicoComponent', () => {
  let component: NaturezaServicoComponent;
  let fixture: ComponentFixture<NaturezaServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaturezaServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaturezaServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
