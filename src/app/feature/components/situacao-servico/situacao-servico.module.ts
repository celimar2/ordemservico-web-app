import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SituacaoServicoComponent} from "./list/situacao-servico.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SituacaoServicoService} from "../../services/situacao-servico.service";
import {SituacaoServicoDetailComponent} from "../situacao-servico/detail/situacao-servico-detail.component";
import {SituacaoServicoRoutingModule} from "../situacao-servico/situacao-servico-routing.module";

@NgModule({
  imports: [CommonModule, SituacaoServicoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [SituacaoServicoComponent, SituacaoServicoDetailComponent],
  providers: [ SituacaoServicoService ]
})
export class SituacaoServicoModule { }
