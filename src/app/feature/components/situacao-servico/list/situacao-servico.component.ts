import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {SituacaoServico} from "../../../models/situacao-servico.model";
import {Router} from "@angular/router";
import {SituacaoServicoService} from "../../../services/situacao-servico.service";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-situacao-servico',
  templateUrl: './situacao-servico.component.html',
  styleUrls: ['./situacao-servico.component.scss']
})
export class SituacaoServicoComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private situacaoServicoService: SituacaoServicoService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  situacaoServicos: SituacaoServico[];
  displayedColumns = [ 'id', 'nome', 'sequencia', 'descricao', 'ativo', 'actionsColumn'];
  dataSource = new MatTableDataSource<SituacaoServico>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.situacaoServicoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("SituacaoServico");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.situacaoServicoService.getList()
      .subscribe( data => {
        this.situacaoServicos = data;
      });
  }

  delete(situacaoServico: SituacaoServico){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.situacaoServicoService.delete(situacaoServico.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == situacaoServico.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });
  };

  show(situacaoServico: SituacaoServico): void {
    if (this.accessRead) {
      this.router.navigate(['situacao-servico/detail'],{ queryParams: { mode:"read", id:situacaoServico.id.toString()} });
    }
  };

  edit(situacaoServico: SituacaoServico): void {
    if (this.accessUpdate) {
      this.router.navigate(['situacao-servico/detail'],{ queryParams: { mode:"update", id:situacaoServico.id.toString()} });
    }
  };

  add(): void {
    this.router.navigate(['situacao-servico/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


