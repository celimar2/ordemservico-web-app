import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SituacaoServicoComponent} from './situacao-servico.component';

describe('SituacaoServicoComponent', () => {
  let component: SituacaoServicoComponent;
  let fixture: ComponentFixture<SituacaoServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituacaoServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituacaoServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
