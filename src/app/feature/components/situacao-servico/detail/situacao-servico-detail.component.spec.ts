import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SituacaoServicoDetailComponent} from './situacao-servico-detail.component';

describe('SituacaoServicoDetailComponent', () => {
  let component: SituacaoServicoDetailComponent;
  let fixture: ComponentFixture<SituacaoServicoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituacaoServicoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituacaoServicoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
