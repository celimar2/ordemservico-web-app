import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {SituacaoServico} from "../../../models/situacao-servico.model";
import {first} from "rxjs/operators";
import {SituacaoServicoService} from "../../../services/situacao-servico.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-situacao-servico-detail',
  templateUrl: './situacao-servico-detail.component.html',
  styleUrls: ['./situacao-servico-detail.component.scss']
})
export class SituacaoServicoDetailComponent implements OnInit {
  constructor(private situacaoServicoService: SituacaoServicoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  situacaoServicoId: string;
  situacaoServico: SituacaoServico;
  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.situacaoServicoId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("SituacaoVeiculo");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;


    this.detailForm = this.formBuilder.group({
      id: [null,{disabled: true}],
      nome: ['', Validators.required],
      descricao: [''],
      sequencia: [0],
      ativo: [false, Validators.required]
    });

    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode != 'insert') {
        this.situacaoServicoService.getById(this.situacaoServicoId)
          .subscribe( data => {
            this.detailForm.setValue(data);
        });
    }
  }


  // onCancel() {
  //   this.router.navigate(['situacaoServico']);
  //   return;
  // }

  onSubmit() {
    if (this.mode == "insert") {
      this.situacaoServicoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['situacao-servico']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.situacaoServicoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['situacao-servico']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}


