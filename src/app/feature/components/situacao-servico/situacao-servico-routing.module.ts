import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SituacaoServicoComponent} from "./list/situacao-servico.component";
import {SituacaoServicoDetailComponent} from "../situacao-servico/detail/situacao-servico-detail.component";


const routes: Routes = [
  { path: "", component: SituacaoServicoComponent},
  { path: "detail", component: SituacaoServicoDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class SituacaoServicoRoutingModule { }
