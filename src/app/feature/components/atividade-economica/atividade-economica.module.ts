import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AtividadeEconomicaRoutingModule} from "../atividade-economica/atividade-economica-routing.module";
import {AtividadeEconomicaComponent} from "./list/atividade-economica.component";
import {AtividadeEconomicaDetailComponent} from "../atividade-economica/detail/atividade-economica-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AtividadeEconomicaService} from "../../services/atividade-economica.service";

@NgModule({
  imports: [CommonModule, AtividadeEconomicaRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [AtividadeEconomicaComponent, AtividadeEconomicaDetailComponent],
  providers: [ AtividadeEconomicaService ]
})
export class AtividadeEconomicaModule { }
