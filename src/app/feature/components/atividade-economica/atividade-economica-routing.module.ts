import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AtividadeEconomicaComponent} from "./list/atividade-economica.component";
import {AtividadeEconomicaDetailComponent} from "../atividade-economica/detail/atividade-economica-detail.component";


const routes: Routes = [
  { path: "", component: AtividadeEconomicaComponent},
  { path: "detail", component: AtividadeEconomicaDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class AtividadeEconomicaRoutingModule { }
