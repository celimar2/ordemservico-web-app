import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AtividadeEconomicaComponent} from './atividade-economica.component';

describe('AtividadeEconomicaComponent', () => {
  let component: AtividadeEconomicaComponent;
  let fixture: ComponentFixture<AtividadeEconomicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtividadeEconomicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadeEconomicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
