import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {Router} from "@angular/router";
import {AtividadeEconomica} from "../../../models/atividade-economica.model";
import {AtividadeEconomicaService} from "../../../services/atividade-economica.service";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-atividade-economica',
  templateUrl: './atividade-economica.component.html',
  styleUrls: ['./atividade-economica.component.scss']
})
export class AtividadeEconomicaComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private atividadeEconomicaService: AtividadeEconomicaService,
              public dialog: MatDialog) {
  }
  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  atividadeEconomicas: AtividadeEconomica[];
  displayedColumns = [ 'id', 'nome', 'ativo', 'actionsColumn'];
  dataSource = new MatTableDataSource<AtividadeEconomica>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.atividadeEconomicaService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("AtividadeEconomica");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.atividadeEconomicaService.getList()
      .subscribe( data => {
        this.atividadeEconomicas = data;
      });
  }

  delete(atividadeEconomica: AtividadeEconomica){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.atividadeEconomicaService.delete(atividadeEconomica.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == atividadeEconomica.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(atividadeEconomica: AtividadeEconomica): void {
    if (this.accessRead) {
      this.router.navigate(['atividade-economica/detail'], {queryParams: {mode: "read", id: atividadeEconomica.id.toString()}});
    }
  };

  edit(atividadeEconomica: AtividadeEconomica): void {
    if (this.accessUpdate) {
      this.router.navigate(['atividade-economica/detail'], {queryParams: {mode: "update", id: atividadeEconomica.id.toString()}});
    }
  };

  add(): void {
    this.router.navigate(['atividade-economica/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


