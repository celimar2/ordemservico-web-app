import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";
import {AtividadeEconomica} from "../../../models/atividade-economica.model";
import {AtividadeEconomicaService} from "../../../services/atividade-economica.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-atividade-economica-detail',
  templateUrl: './atividade-economica-detail.component.html',
  styleUrls: ['./atividade-economica-detail.component.scss']
})
export class AtividadeEconomicaDetailComponent implements OnInit {
  constructor(private atividadeEconomicaService: AtividadeEconomicaService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  atividadeEconomicaId: string;
  atividadeEconomica: AtividadeEconomica;
  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.atividadeEconomicaId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("AtividadeEconomica");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;


    this.detailForm = this.formBuilder.group({
      id: [null,{disabled: true}],
      nome: ['', Validators.required],
      ativo: [false, Validators.required]
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode != 'insert') {
      this.atividadeEconomicaService.getById(this.atividadeEconomicaId)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }

  onCancel() {
    this.router.navigate(['atividadeEconomica']);
    return;
  }

  onSubmit() {

    if (this.mode == "insert") {
      this.atividadeEconomicaService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['atividade-economica']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.atividadeEconomicaService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['atividade-economica']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}


