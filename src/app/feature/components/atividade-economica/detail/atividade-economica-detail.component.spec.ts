import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AtividadeEconomicaDetailComponent} from './atividade-economica-detail.component';

describe('AtividadeEconomicaDetailComponent', () => {
  let component: AtividadeEconomicaDetailComponent;
  let fixture: ComponentFixture<AtividadeEconomicaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtividadeEconomicaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadeEconomicaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
