import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MarcaVeiculoComponent} from './marca-veiculo.component';

describe('MarcaVeiculoComponent', () => {
  let component: MarcaVeiculoComponent;
  let fixture: ComponentFixture<MarcaVeiculoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcaVeiculoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaVeiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
