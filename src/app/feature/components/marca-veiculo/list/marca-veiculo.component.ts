import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {Router} from "@angular/router";
import {MarcaVeiculo} from "../../../models/marca-veiculo.model";
import {MarcaVeiculoService} from "../../../services/marca-veiculo.service";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {Regiao} from "../../../models/regiao.model";

@Component({
  selector: 'app-marca-veiculo',
  templateUrl: './marca-veiculo.component.html',
  styleUrls: ['./marca-veiculo.component.scss']
})
export class MarcaVeiculoComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private marcaVeiculoService: MarcaVeiculoService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  marcaVeiculos: MarcaVeiculo[];

  displayedColumns = [ 'id', 'nome', 'sigla', 'actionsColumn'];

  dataSource = new MatTableDataSource<MarcaVeiculo>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.marcaVeiculoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("MarcaVeiculo");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.marcaVeiculoService.getList()
      .subscribe( data => {
        this.marcaVeiculos = data;
      });
  }

  delete(marcaVeiculo: MarcaVeiculo){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.marcaVeiculoService.delete(marcaVeiculo.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == marcaVeiculo.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(marcaVeiculo: MarcaVeiculo): void {
    if (this.accessRead) {
      this.router.navigate(['marca-veiculo/detail'], {queryParams: {mode: "read", id: marcaVeiculo.id.toString()}});
    }
  };


  edit(marcaVeiculo: MarcaVeiculo): void {
    if (this.accessUpdate) {
      this.router.navigate(['marca-veiculo/detail'], {queryParams: {mode: "update", id: marcaVeiculo.id.toString()}});
    }
  };

  add(): void {
    this.router.navigate(['marca-veiculo/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


