import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MarcaVeiculoDetailComponent} from './marca-veiculo-detail.component';

describe('MarcaVeiculoDetailComponent', () => {
  let component: MarcaVeiculoDetailComponent;
  let fixture: ComponentFixture<MarcaVeiculoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcaVeiculoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaVeiculoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
