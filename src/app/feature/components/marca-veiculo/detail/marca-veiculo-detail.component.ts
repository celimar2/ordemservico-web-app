import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MarcaVeiculo} from "../../../models/marca-veiculo.model";
import {MarcaVeiculoService} from "../../../services/marca-veiculo.service";
import {first} from "rxjs/operators";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-marca-veiculo-detail',
  templateUrl: './marca-veiculo-detail.component.html',
  styleUrls: ['./marca-veiculo-detail.component.scss']
})
export class MarcaVeiculoDetailComponent implements OnInit {

  constructor(private marcaVeiculoService: MarcaVeiculoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  marcaVeiculoId: string;
  marcaVeiculo: MarcaVeiculo;
  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.marcaVeiculoId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("MarcaVeiculo");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;


    this.detailForm = this.formBuilder.group({
      id: [null,{disabled: true}],
      nome: ['', Validators.required],
      sigla: ['']
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode != 'insert') {
      this.marcaVeiculoService.getById(this.marcaVeiculoId)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }

  onCancel() {
    this.router.navigate(['marcaVeiculo']);
    return;
  }

  onSubmit() {
    if (this.mode == "insert") {
      this.marcaVeiculoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['marca-veiculo']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.marcaVeiculoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['marca-veiculo']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}


