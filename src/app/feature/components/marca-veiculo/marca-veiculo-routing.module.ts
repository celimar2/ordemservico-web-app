import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {MarcaVeiculoComponent} from "./list/marca-veiculo.component";
import {MarcaVeiculoDetailComponent} from "../marca-veiculo/detail/marca-veiculo-detail.component";


const routes: Routes = [
  { path: "", component: MarcaVeiculoComponent},
  { path: "detail", component: MarcaVeiculoDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class MarcaVeiculoRoutingModule { }
