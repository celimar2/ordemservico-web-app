import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MarcaVeiculoRoutingModule} from "../marca-veiculo/marca-veiculo-routing.module";
import {MarcaVeiculoComponent} from "./list/marca-veiculo.component";
import {MarcaVeiculoDetailComponent} from "../marca-veiculo/detail/marca-veiculo-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MarcaVeiculoService} from "../../services/marca-veiculo.service";

@NgModule({
  imports: [CommonModule, MarcaVeiculoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [MarcaVeiculoComponent, MarcaVeiculoDetailComponent],
  providers: [ MarcaVeiculoService ]
})
export class MarcaVeiculoModule { }
