import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FornecedorComponent} from "./list/fornecedor.component";
import {FornecedorDetailComponent} from "./detail/fornecedor-detail.component";
import {FornecedorCadastrarComponent} from "./cadastrar/fornecedor-cadastrar.component";

const routes: Routes = [
  { path: "", component: FornecedorComponent},
  { path: "detail", component: FornecedorDetailComponent},
  { path: "cadastrar", component: FornecedorCadastrarComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class FornecedorRoutingModule { }
