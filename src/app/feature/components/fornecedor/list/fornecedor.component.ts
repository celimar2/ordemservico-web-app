import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Router} from "@angular/router";
import {FornecedorService} from "../../../services/fornecedor.service";
import {Fornecedor} from "../../../models/fornecedor.model";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-fornecedor',
  templateUrl: './fornecedor.component.html',
  // styleUrls: ['./fornecedor.component.scss']
})
export class FornecedorComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private fornecedorService: FornecedorService,
              public dialog: MatDialog) {}

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  fornecedors: Fornecedor[];
  displayedColumns = ['id', 'nomeRazaoSocial', 'apelidoNomeFantasia', 'cpfCnpj', 'ativo', 'actionsColumn'];
  dataSource = new MatTableDataSource<Fornecedor>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.fornecedorService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Fornecedor");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.fornecedorService.getList()
      .subscribe( data => {
        this.fornecedors = data;
      });
  }

  delete(fornecedor: Fornecedor){
    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fornecedorService.delete(fornecedor.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == fornecedor.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(fornecedor: Fornecedor): void {
    if (this.accessRead) {
      this.router.navigate(['fornecedor/detail'], {queryParams: {mode: "read", id: fornecedor.id.toString()}});
      this.loadData();
    }
  };

  edit(fornecedor: Fornecedor): void {
    if (this.accessUpdate) {
      this.router.navigate(['fornecedor/detail'], {queryParams: {mode: "update", id: fornecedor.id.toString()}});
      this.loadData();
    }
  };

  add(): void {
    this.router.navigate(['fornecedor/cadastrar']);
    this.loadData();
  };

}

