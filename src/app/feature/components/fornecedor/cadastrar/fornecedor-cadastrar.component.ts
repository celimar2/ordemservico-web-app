import {Component, OnInit} from '@angular/core';
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {FornecedorService} from "../../../services/fornecedor.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Globals} from "../../../../shared/globals";
import {Fornecedor} from "../../../models/fornecedor.model";

@Component({
  selector: 'app-fornecedor-cadastrar',
  templateUrl: './fornecedor-cadastrar.component.html',
  // styleUrls: ['./fornecedor-cadastrar.component.scss']
})
export class FornecedorCadastrarComponent implements OnInit {

  constructor(private router: Router,
              private fb: FormBuilder,
              private enumService: EnumsService,
              private fornecedorService:  FornecedorService,
              private globals: Globals) {}

  detailForm: FormGroup;

  public fornecedorResult: Fornecedor = null;
  public cpfCnpj: string = "";
  public tiposJuridicos: any[];
  public cpfMask: any;
  public cnpjMask: any;

  ngOnInit() {

    this.cpfMask = this.globals.CPF_MASK;
    this.cnpjMask = this.globals.CNPJ_MASK;

    this.enumService.getTipoJuridico().subscribe(
    data => {this.tiposJuridicos = this.enumService.enumToArray(data);}
    );
    this.detailForm = this.fb.group({
      tipoJur: ['PJ', [Validators.required]],
      cpfCnpj: ['', [Validators.required, Validators.pattern(this.globals.CPF_CNPJ_VALIDATOR)]],
    })
  }

  cancel(): void {
    this.router.navigate(['fornecedor']);
  }

  submit(form) {
    const cpfCnpjRequest = {value: this.detailForm.get('cpfCnpj').value};
    this.fornecedorService.getByCpfCnpj(cpfCnpjRequest)
      .subscribe(
        data => {
          this.fornecedorResult = data;
          if (this.fornecedorResult == null) {
            this.router.navigate(
              ['fornecedor/detail'], {
                queryParams: {
                  mode: "insert",
                  id: null,
                  tipojur: this.detailForm.get('tipoJur').value,
                  cpfcnpj: this.detailForm.get('cpfCnpj').value
                }
              }
            );
          }
        },
        error => {
          alert(error);
           // let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        }
      )
  }

}

