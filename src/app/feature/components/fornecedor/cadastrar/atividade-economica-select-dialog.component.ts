import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AtividadeEconomica} from "../../../models/atividade-economica.model";

@Component({
  selector: 'atividade-economica-select-dialog',
  templateUrl: './atividade-economica-select-dialog.component.html'
})
export class AtividadeEconomicaSelectDialogComponent{

  dialogForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<AtividadeEconomicaSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AtividadeEconomica[]) {
  }

  ngOnInit() {
      this.dialogForm = this.fb.group({

      })
  }

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  // submit(form) {
  //   this.dialogRef.close(`${role}`);
  // }

  confirm(atividade) {
    this.dialogRef.close(`${atividade}`);
  }

  }
