import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {first} from "rxjs/operators";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Fornecedor} from "../../../models/fornecedor.model";
import {FornecedorService} from "../../../services/fornecedor.service";
import {Estado} from "../../../models/estado.model";
import {Municipio} from "../../../models/municipio.model";
import {EstadoService} from "../../../services/estado.service";
import {Globals} from "../../../../shared/globals";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {MatDatepicker, MatDialog} from "@angular/material";
import * as moment from 'moment';
import {Moment} from 'moment';
import {AtividadeEconomicaService} from "../../../services/atividade-economica.service";
import {AtividadeEconomica} from "../../../models/atividade-economica.model";
import {AtividadeEconomicaSelectDialogComponent} from "../cadastrar/atividade-economica-select-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

//TODO
//inserir validação de cpf cnpj
//inserir mascara cpf cnpj

@Component({
  selector: 'app-fornecedor-detail',
  templateUrl: './fornecedor-detail.component.html',
  styleUrls: ['./fornecedor-detail.component.scss']
})
export class FornecedorDetailComponent implements AfterViewInit, OnInit {

  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;

  constructor(private fornecedorService: FornecedorService,
              private estadoService: EstadoService,
              private enumService: EnumsService,
              private ativEconService: AtividadeEconomicaService,
              private globals: Globals,
              private fb: FormBuilder,
              private auth: AuthService,
              public dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute) {
  }

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  isValidMoment: boolean = false;
  sub: any;
  loading: boolean = false;
  mode: string;
  fornecedorId: string;
  fornecedor: Fornecedor;
  detailForm: FormGroup;
  estados: Estado[] = [];
  municipios: Municipio[] = [];
  msg: any;
  allAtividades: AtividadeEconomica[];
  tipoJurNew: string = "PJ";
  cpfCnpjNew: string = "";


  public generos: any[];
  public tiposJuridico: any[];
  public tiposTelefone: any[];

  public cpfMask: any;
  public cnpjMask: any;
  public cepMask: any;
  public telMask: any;

  ngAfterViewInit() {
    this.picker._selectedChanged.subscribe(
      (newDate: Moment) => {this.isValidMoment = moment.isMoment(newDate);},
      (error) => {throw Error(error);}
    );

  }

  ngOnInit() {

    this.sub = this.route.queryParams
      .subscribe(
        params => {
          this.mode = params['mode'] || "";
          this.fornecedorId = params['id'] || "0";
          this.tipoJurNew = params['tipojur'] || "PJ";
          this.cpfCnpjNew = params['cpfcnpj'] || "";
      });
    this.menuAccess = this.auth.getMenuAccess("Fornecedor");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.loadAuxData();

    this.initForm();

    if (this.mode != 'insert') {
      this.fornecedorService.getById(this.fornecedorId)
        .subscribe(resp => {
          const telefones = this.detailForm.get('telefones') as FormArray;
          while (telefones.length) {
            telefones.removeAt(0);
          }

          const ativEconList = this.detailForm.get('atividadesEconomicas') as FormArray;
          while (ativEconList.length) {
            ativEconList.removeAt(0);
          }

          this.detailForm.patchValue(resp);
          resp.telefones.forEach(tel =>
            telefones.push(this.fb.group(tel))
          );

          // add form array values in a loop
          resp.atividadesEconomicas.forEach(ativ =>
            ativEconList.push(this.fb.group(ativ))
          );

          this.fornecedor = resp;
          this.selectMunicipioPorEstado(this.fornecedor.estado);
        },
        error => this.msg = <any>error)
    }

  }

  loadAuxData() {
    this.cpfMask = this.globals.CPF_MASK;
    this.cnpjMask = this.globals.CNPJ_MASK;
    this.cepMask = this.globals.CEP_MASK;
    this.telMask = this.globals.TEL_MASK;
    this.loading = true;
    this.estadoService.getList().subscribe(
      data => {this.estados = data;},
        () => {this.loading = false;}
      );

    this.loading = true;
    this.enumService.getGenero().subscribe(
      data => {this.generos = this.enumService.enumToArray(data);},
      () => {this.loading = false;}
      );

    this.loading = true;
    this.enumService.getTipoJuridico().subscribe(
      data => {this.tiposJuridico = this.enumService.enumToArray(data);},
      () => {this.loading = false;}
      );

    this.loading = true;
    this.enumService.getTipoTelefone().subscribe(
      data => {this.tiposTelefone = this.enumService.enumToArray(data);},
      () => {this.loading = false;}
      );

    this.loading = true;
    this.ativEconService.getListAtivos().subscribe(
      resp => {this.allAtividades = resp;},
      () => {this.loading = false;}
      );
  }

  initForm() {
    this.detailForm = this.fb.group({
      id: 0,
      tipoJuridico: [this.tipoJurNew, Validators.required],
      cpfCnpj: [this.cpfCnpjNew, [Validators.required, Validators.pattern(this.globals.CPF_CNPJ_VALIDATOR)]],
      nomeRazaoSocial: ['', Validators.required],
      apelidoNomeFantasia: ['', Validators.required],
      identidadeInscricaoEstadual: ['', Validators.required],
      genero: '',
      nascimentoAbertura: null,
      email: ['', Validators.email],
      ativo: true,
      notas: '',
      cep: ['', Validators.pattern(this.globals.CEP_VALIDATOR)],
      logradouro: ['', Validators.required],
      complemento: '',
      bairro: '',
      estado: [null, Validators.required],
      municipio: [null, Validators.required],
      telefones: this.fb.array([]),
      atividadesEconomicas: this.fb.array([])
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

  }

  // ---------------------------------------------------------------------------
  // telefones
  // ---------------------------------------------------------------------------

  initFormTelefone() {
    return this.fb.group({
      tipo: ['', Validators.required],
      numero: ['', [Validators.required, Validators.pattern(this.globals.TEL_VALIDATOR)]],
    });
  }

  selectMunicipioPorEstado(estado) {
    this.loading = true;
    this.estadoService.getMunicipioByUf(estado.uf).subscribe(
      data => {this.municipios = data;},
        () => {this.loading = false;}
        );
  }

  removeTelefone(idx: number) {
    const telefoneList = <FormArray>this.detailForm.controls['telefones'];
    telefoneList.removeAt(idx);
  }

  addTelefone(): void {
    const telefoneList = <FormArray>this.detailForm.controls['telefones'];
    const newTelefone = this.initFormTelefone();
    telefoneList.push(newTelefone);
  }

  // ---------------------------------------------------------------------------
  // atividades economicas
  // ---------------------------------------------------------------------------

  addAtividadesItem(ae: AtividadeEconomica) {
    return this.fb.group({
      id: ae.id,
      nome: ae.nome,
      ativo: ae.ativo
    });
  }

  removeAtividade(idx: number) {
    const ativEconArray = <FormArray>this.detailForm.controls['atividadesEconomicas'];
    ativEconArray.removeAt(idx);
  }

  addAtividade(): void {
    const userAtividades = this.detailForm.get('atividadesEconomicas').value;
    const diffAtividades = this.allAtividades.filter(
      function (obj) {
        return !userAtividades.some(
          function (obj2) {
            return obj.id == obj2.id;
          }
        );
      });

    const dialogRef = this.dialog.open(AtividadeEconomicaSelectDialogComponent,
      {width: '350px', data: diffAtividades}
    );

    dialogRef.afterClosed().subscribe(
      result => {
        const atividades = <FormArray>this.detailForm.controls['atividadesEconomicas'];
        const newAtividade = this.addAtividadesItem(result.value);
        atividades.push(newAtividade);
      }
    );
  }

  // ---------------------------------------------------------------------------


  onSubmit() {

    if (this.detailForm.value.tipoJuridico = "PJ") {
      this.detailForm.value.genero = "OUT";
    }

    if (this.mode == "insert") {

      this.fornecedorService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['fornecedor']);
            return;
          },
          error => {
            alert(error.mwssage);
          });

    } else if (this.mode == "update") {
      this.fornecedorService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['fornecedor']);
            return;
          },
          error => {
            alert(error.mwssage);
          });
    }
  }


  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

}
