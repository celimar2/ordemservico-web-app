import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FornecedorRoutingModule} from "../fornecedor/fornecedor-routing.module";
import {FornecedorComponent} from "./list/fornecedor.component";
import {FornecedorDetailComponent} from "../fornecedor/detail/fornecedor-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FornecedorService} from "../../services/fornecedor.service";
import {FornecedorCadastrarComponent} from "./cadastrar/fornecedor-cadastrar.component";

@NgModule({
  imports: [CommonModule, FornecedorRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [FornecedorComponent, FornecedorCadastrarComponent, FornecedorDetailComponent],
  providers: [ FornecedorService ]
})
export class FornecedorModule { }
