import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilialRoutingModule} from "../filial/filial-routing.module";
import {FilialComponent} from "./list/filial.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FilialService} from "../../services/filial.service";
import {FilialDetailComponent} from "./detail/filial-detail.component";

@NgModule({
  imports: [CommonModule, FilialRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [FilialComponent, FilialDetailComponent],
  providers: [ FilialService ]
})
export class FilialModule { }
