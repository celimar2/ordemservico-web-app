import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FilialComponent} from "./list/filial.component";
import {FilialDetailComponent} from "./detail/filial-detail.component";


const routes: Routes = [
  { path: "", component: FilialComponent},
  { path: "detail", component: FilialDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class FilialRoutingModule { }
