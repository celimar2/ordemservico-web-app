import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportRoutingModule} from './report-routing.module';
import {ReportVeiculoListagemComponent} from './report-veiculo-listagem/report-veiculo-listagem.component';
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LocalCurrencyPipe} from "../../../shared/pipes/local-currency.pipe";
import {VeiculoService} from "../../services/veiculo.service";

@NgModule({
  imports: [CommonModule, ReportRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  providers: [ VeiculoService, LocalCurrencyPipe],
  declarations: [ReportVeiculoListagemComponent]
})
export class ReportModule { }
