import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReportVeiculoListagemComponent} from "./report-veiculo-listagem/report-veiculo-listagem.component";

const routes: Routes = [
  { path: "veiculo-listagem", component: ReportVeiculoListagemComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
