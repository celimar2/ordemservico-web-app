import {Component, OnInit} from '@angular/core';
import {VeiculoService} from "../../../services/veiculo.service";
import {VeiculoListagem} from "../../../models/veiculo-listagem.model";
import * as moment from "moment";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import {EnumModel} from "../../../../core/models/enum.model";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {Globals} from "../../../../shared/globals";
import {Totais} from "../../../models/Totais";


@Component({
  selector: 'app-report-veiculo-listagem',
  templateUrl: './report-veiculo-listagem.component.html',
  styleUrls: ['./report-veiculo-listagem.component.scss']
})
export class ReportVeiculoListagemComponent implements OnInit {

  constructor(private veiculoService: VeiculoService,
              private globals: Globals,
              private enumService: EnumsService,
              private formBuilder: FormBuilder) {
  }
  totais: Totais[] = [];
  veiculos: VeiculoListagem[] = [];
  veiculosList: VeiculoListagem[];
  detailForm: FormGroup;
  selectedOptions: any[];
  filtros: string[];
  public situacoes: {key: string; value: string; idx: number}[];

  ngOnInit() {
    this.enumService.getSituacaoVeiculo()
      .subscribe(data => {
        this.situacoes = this.enumService.enumToArray(data)
          .sort((a: EnumModel, b: EnumModel) => a.key > b.key ? 1 : a.key < b.key ? -1 : 0);
        this.situacoes.splice(this.situacoes.findIndex(value => value.value == "Selecione"),1);
      });

    this.detailForm = this.formBuilder.group({
      situacoes: new FormControl([]),
      tipoReport: "analitico",
      dataIni: [Date, Validators.required],
      dataFim: [Date, Validators.required],
    });

    let monthBefore = moment().subtract(30, 'days').toDate()
    let now = moment().toDate();
    this.detailForm.controls['dataIni'].setValue( monthBefore );
    this.detailForm.controls['dataFim'].setValue( now );
    this.detailForm.controls['situacoes'].setValue( []);

    this.filtrar();
  }

  onSituacaoChange(list) {
    this.selectedOptions = list.selectedOptions.selected.map(item => item.value);

  }

  filtrar():void {

    let dtIni = moment(this.detailForm.controls['dataIni'].value).format(this.globals.DATE_FMT);
    let dtFim = moment(this.detailForm.controls['dataFim'].value).format(this.globals.DATE_FMT);
    let tipo = this.detailForm.get('tipoReport').value;

    this.filtros = [];
    this.filtros.push("Período: "+dtIni+ " até "+dtFim);
    if (this.detailForm.get('tipoReport').value == 'analitico') {
      this.filtros.push("\nSituacões: "+this.formatSituacoes(this.detailForm.get('situacoes').value) );
    }

    if (tipo == "analitico") {
      this.veiculoService.getListWithParams(dtIni, dtFim,  this.detailForm.controls['situacoes'].value)
        .subscribe(data => {
          this.veiculos = data;
        });

    } else {
      this.veiculoService.getTotalsWithParams(dtIni, dtFim)
        .subscribe(data => {
          // console.log(JSON.stringify(data));
          this.totais = data;
        });
    }
  }

  formatSituacoes(selecionados: any[]): string {
    let result: string = '';
    selecionados.forEach( x =>
      result = result + ', ' + this.situacoes.find(v => v.key == x).value.replace("_", " ")
    );
    result = result.substr(2);
    return result;
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank');
    //popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
      <title>Listagem de veículos</title>
      <link rel="stylesheet" type="text/css" media="print" href="css/print.css">
      </head>
      <body onload="window.print();window.close()">
        ${printContents}
      </body>
    </html>`
    );
    popupWin.document.close();

    // <link rel="stylesheet" type="text/css" href="../../../../../../../style.css" />

    // let printContents = document.getElementById('print-section').innerHTML;
    // let originalContents = document.body.innerHTML;
    // document.body.innerHTML = printContents;
    // window.print();
    // document.body.innerHTML = originalContents;
    // document.getElementById('print-section').print();

  }

  public exportToPdf()
  {
    var data = document.getElementById('print-section');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 195;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4');
      var position = 10;
      pdf.addImage(contentDataURL, 'PNG', 10, position, imgWidth, imgHeight)
      pdf.save('Listagem_Veiculos.pdf');
    });
    // const pdf = new jsPDF('p', 'mm', 'a4');
    // var elementHandler = {
    //   '#ignorePDF': function (element, renderer) {
    //     return true;
    //   }
    // };
    // pdf.addHTML(data, () => {
    // pdf.fromHTML(data, 15, 15,
    //   {'width': 180,
    //     'elementHandlers': elementHandler
    // });
    // pdf.save('web.pdf');
  }
}
