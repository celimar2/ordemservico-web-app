import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrdemServicoRoutingModule} from './ordem-servico-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {OrdemServicoComponent} from "./list/ordem-servico.component";
import {OrdemServicoDetailComponent} from "./detail/ordem-servico-detail.component";
import {OrdemServicoService} from "../../services/ordem-servico.service";
import {LocalCurrencyPipe} from "../../../shared/pipes/local-currency.pipe";
import {LocalCurrencyDirective} from "../../../shared/directives/local-currency.directive";

@NgModule({
  imports: [CommonModule, OrdemServicoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [OrdemServicoComponent, OrdemServicoDetailComponent, LocalCurrencyDirective],
  providers: [ OrdemServicoService, LocalCurrencyPipe],

})
export class OrdemServicoModule { }
