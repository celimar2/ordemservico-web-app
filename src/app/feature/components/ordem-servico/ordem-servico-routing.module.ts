import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OrdemServicoDetailComponent} from "./detail/ordem-servico-detail.component";
import {OrdemServicoComponent} from "./list/ordem-servico.component";

const routes: Routes = [
  { path: "", component: OrdemServicoComponent},
  { path: "detail", component: OrdemServicoDetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdemServicoRoutingModule { }
