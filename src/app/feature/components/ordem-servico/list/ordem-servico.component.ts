import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {OrdemServicoService} from "../../../services/ordem-servico.service";
import {OrdemServico} from "../../../models/ordem-servico.model";
import {Router} from "@angular/router";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {OrdemServicoListagem} from "../../../models/ordem-servico-listagem.model.ts";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-ordem-servico',
  templateUrl: './ordem-servico.component.html',
  styleUrls: ['./ordem-servico.component.scss']
})
export class OrdemServicoComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router,
              private auth: AuthService,
              private ordemServicoService: OrdemServicoService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  ordemServicos: OrdemServicoListagem[];
  displayedColumns = [ 'id', 'veiculo', 'fornecedor', 'situacao', 'retirada', 'devolucao', 'actionsColumn'];
  dataSource = new MatTableDataSource<OrdemServicoListagem>();


  ngAfterViewInit() {
    this.ordemServicoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("OrdemServico");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.ordemServicoService.getList()
      .subscribe( data => {
        this.ordemServicos = data;
      });
  }

  delete(ordemServico: OrdemServico){
    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ordemServicoService.delete(ordemServico.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex(x => x.id == ordemServico.id);
            this.dataSource.data.splice(idx, 1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });
  };

  show(ordemServico: OrdemServico): void {
    if (this.accessRead) {
      this.router.navigate(['ordem-servico/detail'], {queryParams: {mode: "read", id: ordemServico.id.toString()}});
    }
  };

  edit(ordemServico: OrdemServico): void {
    if (this.accessUpdate) {
      this.router.navigate(['ordem-servico/detail'], {queryParams: {mode: "update", id: ordemServico.id.toString()}});
    }
  };

  add(): void {
    this.router.navigate(['ordem-servico/detail'],{ queryParams: { mode:"insert"} });
    this.loadData();
  };

}


