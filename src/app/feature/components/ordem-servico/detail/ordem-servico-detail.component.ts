import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDatepicker, MatDialog} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {Globals} from "../../../../shared/globals";
import {first, map, startWith} from "rxjs/operators";
import {AtividadeEconomicaService} from "../../../services/atividade-economica.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';
import {Moment} from 'moment';
import {OrdemServicoService} from "../../../services/ordem-servico.service";
import {OrdemServico} from "../../../models/ordem-servico.model";
import {SituacaoServicoService} from "../../../services/situacao-servico.service";
import {VeiculoService} from "../../../services/veiculo.service";
import {Veiculo} from "../../../models/veiculo.model";
import {Observable} from "rxjs/Observable";
import {FornecedorService} from "../../../services/fornecedor.service";
import {Fornecedor} from "../../../models/fornecedor.model";
import {NaturezaServicoService} from "../../../services/natureza-servico.service";
import {Location} from "@angular/common";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-ordem-servico-detail',
  templateUrl: './ordem-servico-detail.component.html',
  styleUrls: ['./ordem-servico-detail.component.scss']
})
export class OrdemServicoDetailComponent implements OnInit {

  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;

  constructor(private ordemServicoService: OrdemServicoService,
              private situacaoService: SituacaoServicoService,
              private naturezaServico: NaturezaServicoService,
              private veiculoService: VeiculoService,
              private fornecedorService: FornecedorService,
              private ativEconService: AtividadeEconomicaService,
              private _location: Location,
              private globals: Globals,
              private fb: FormBuilder,
              public dialog: MatDialog,
              private router: Router,
              private auth: AuthService,
              private route: ActivatedRoute) {
  }

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  isValidMoment: boolean = false;
  sub: any;
  loading: boolean = false;
  mode: string;
  ordemServicoId: string;
  ordemServico: OrdemServico;
  detailForm: FormGroup;
  situacoes: any[] = [];
  naturezas: any[] = [];
  msg: any;
  filteredVeiculos: Observable<Veiculo[]>;
  filteredFornecedores: Observable<Fornecedor[]>;
  veiculos: Veiculo[] = [];
  fornecedores: Fornecedor[] = [];
  proxyValue: any;

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(
      params => {
        this.mode = params['mode'] || "";
        this.ordemServicoId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("OrdemServico");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.loadAuxData();
    this.initForm();
    if (this.mode != 'insert') {
      this.ordemServicoService.getById(this.ordemServicoId)
        .subscribe(resp => {
            const ocorrencias = this.detailForm.get('ocorrencias') as FormArray;
            while (ocorrencias.length) {
              ocorrencias.removeAt(0);
            }
            resp.ocorrencias.forEach(ocr =>
              ocorrencias.push(this.fb.group(ocr))
            );
            this.detailForm.patchValue(resp);
            this.ordemServico = resp;
          },
          error => this.msg = <any>error)
    }
  }

  displayVeiculoFn(veiculo?: Veiculo): string | undefined {
    return veiculo ? veiculo.placa +'  '+ veiculo.marca.nome +'  '+ veiculo.modelo +'  '+ veiculo.cor: undefined;
  }

  private _filterVeiculo(placa: string): Veiculo[] {
    const filterValue = placa.toUpperCase();
    // return this.veiculos.filter( veic => veic.placa.toUpperCase().includes(filterValue) );
    // return this.veiculos.filter(veic => veic.placa.toUpperCase().indexOf(filterValue) === 0);
    return this.veiculos.filter(veic => (veic.placa +' '+ veic.modelo +' '+ veic.cor +' '+ veic.marca.nome).toUpperCase().indexOf(filterValue) >= 0);
  }

  displayFornecedorFn(fornecedor?: Fornecedor): string | undefined {
    if (fornecedor != undefined && fornecedor != null) {
      let atividades = ' ';
      if (fornecedor.atividadesEconomicas.length > 0 ) {
        atividades = ': ';
        fornecedor.atividadesEconomicas.forEach(atv => atividades + atv.nome+', ');
      }
      return fornecedor.apelidoNomeFantasia + atividades;
    } else {
      return undefined;
    }
  }

  private _filterFornecedor(apelidoNomeFantasia: string): Fornecedor[] {
    const filterValue = apelidoNomeFantasia.toUpperCase();
    // return this.fornecedores.filter(forn => forn.apelidoNomeFantasia.toUpperCase().includes(filterValue) );
    // return this.fornecedores.filter(forn => forn.apelidoNomeFantasia.toUpperCase().indexOf(filterValue) === 0);
    return this.fornecedores.filter(forn => forn.apelidoNomeFantasia.toUpperCase().indexOf(filterValue) >= 0);
  }


  loadAuxData() {
    this.loading = true;
    this.naturezaServico.getList()
      .subscribe( data => {
        this.naturezas = data
          .sort((a, b) => a.nome > b.nome ? 1 : a.nome < b.nome ? -1 : 0 );
      }
    )
    this.veiculoService.getAll().subscribe(
      data => {this.veiculos = data
        .sort( (a:Veiculo, b:Veiculo) =>
          a.placa < b.placa ? -1 : a.placa > b.placa ? 1 : 0);},
      () => {this.loading = false;}
    );

    this.fornecedorService.getList().subscribe(
      data => {this.fornecedores = data
        .sort((a:Fornecedor, b:Fornecedor) =>
        a.apelidoNomeFantasia < b.apelidoNomeFantasia ? -1 :
          a.apelidoNomeFantasia > b.apelidoNomeFantasia? 1 : 0);
      },
      () => {this.loading = false;}
    );

    this.loading = true;
    this.situacaoService.getList().subscribe(
      data => {
        data.forEach( sit => this.situacoes.push(sit));
      },
      () => {this.loading = false;}
    );

  }

  initForm() {
    this.detailForm = this.fb.group({
      id: [0],
      veiculo :[null, Validators.required],
      descricao: ['', Validators.required],
      abertura: [moment().format(this.globals.DATETIME_FMT), Validators.required],
      naturezaServico: [null],
      devolucaoPrevisao: [null],
      fornecedor: [null, Validators.required],
      numeroOsFornecedor: [null],
      // filial: [null],
      situacaoServico: [null, Validators.required],
      retiradaDataHora: [null],
      retiradaValorPrevisto: [null],
      retiradaObservacoes: [null],
      retiradaResponsavelColaborador: [null],
      retiradaResponsavelFornecedor: [null],

      devolucaoDataHora: [null],
      devolucaoValorRealizado: [null],
      devolucaoObservacoes: [null],
      devolucaoResponsavelColaborador: [null],
      devolucaoResponsavelFornecedor: [null],
      ocorrencias: this.fb.array([]),
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    this.filteredVeiculos = this.detailForm.get('veiculo').valueChanges
      .pipe(
        startWith<string | Veiculo>(''),
        map(value => typeof value === 'string' ? value : value.placa),
        map(placa => placa ? this._filterVeiculo(placa) : this.veiculos.slice())
      );

    this.filteredFornecedores = this.detailForm.get('fornecedor').valueChanges
      .pipe(
        startWith<string | Fornecedor>(''),
        map(value => typeof value === 'string' ? value : value.apelidoNomeFantasia),
        map(apelidoNomeFantasia => apelidoNomeFantasia ? this._filterFornecedor(apelidoNomeFantasia) : this.fornecedores.slice())
      );

  }

  onSelectionVeiculoChanged(event$) {
    this.proxyValue = event$.option.value.placa;
  }

  getPlaca(value?) {
    const id = value;
    // const this.filteredOptions = 'can't be accessed'
    return value.placa || null;
  }

  onSelectionFornecChanged(event$) {
    this.proxyValue = event$.option.value.nome;
  }

  getNome(value?) {
    const id = value;
    return value.nome|| null;
  }


  ngAfterViewInit() {
    this.picker._selectedChanged.subscribe(
      (newDate: Moment) => {
        this.isValidMoment = moment.isMoment(newDate);
      },
      (error) => {
        throw Error(error);
      }
    );
  }

  // ---------------------------------------------------------------------------
  // Ocorrencias
  // ---------------------------------------------------------------------------

  initFormOcorrencias() {
    return this.fb.group({
      id: null,
      // ordemServicoId: this.detailForm.get('id').value,
      data: null, // [moment().format(this.globals.DATETIME_FMT), Validators.required],
      valor: ['0'],
      observacao: ['', [Validators.required]],
    });
  }

  public removeOcorrencia(idx: number) {
    const ocorrenciaList = <FormArray>this.detailForm.controls['ocorrencias'];
    ocorrenciaList.removeAt(idx);
  }

  public addOcorrencia(): void {
    const ocorrenciaList = <FormArray>this.detailForm.controls['ocorrencias'];
    const newOcorrencia = this.initFormOcorrencias();
    ocorrenciaList.push(newOcorrencia);
  }

  // ---------------------------------------------------------------------------

  formatData(formData){
    if (this.detailForm.get(formData).value != null && this.detailForm.get(formData).value != undefined) {
      this.detailForm.get(formData)
        .setValue(moment(this.detailForm.get(formData).value )
          .format(this.globals.DATETIME_FMT)
        ) ;
    }
  }

  preUpdateData() {
    this.formatData('abertura');
    this.formatData('retiradaDataHora');
    this.formatData('devolucaoDataHora');
    // alert(JSON.stringify(this.detailForm.get('ocorrencias').value));
    if (this.detailForm.get('ocorrencias').value != null && this.detailForm.get('ocorrencias').value != undefined ) {

      let ocr: any = null;
      for (ocr in this.detailForm.get('ocorrencias').value) {
        if (ocr.data != null && ocr.data != undefined) {
          ocr.data = moment(ocr.data ).format(this.globals.DATETIME_FMT);
        }
      }
    }
    // alert("After:\n"+JSON.stringify(this.detailForm.get('ocorrencias').value));
  }

  cancel(): void {
    this.router.navigate(['ordem-servico']);
  }

  onSubmit() {
    this.preUpdateData();

    if (this.mode == "insert") {

      this.ordemServicoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this._location.back();
            // this.router.navigate(['ordem-servico']);
            return;
          },
          error => {
            alert(error.message);
          });

    } else if (this.mode == "update") {
      this.ordemServicoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this._location.back();
            // this.router.navigate(['ordem-servico']);
            return;
          },
          error => {
            alert(error.message);
          });
    }
  }


  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

}
