import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EmpresaRoutingModule} from "../empresa/empresa-routing.module";
import {EmpresaComponent} from "./list/empresa.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {EmpresaService} from "../../services/empresa.service";
import {EmpresaDetailComponent} from "./detail/empresa-detail.component";

@NgModule({
  imports: [CommonModule, EmpresaRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [EmpresaComponent, EmpresaDetailComponent],
  providers: [ EmpresaService ]
})
export class EmpresaModule { }
