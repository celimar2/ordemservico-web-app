import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {EmpresaComponent} from "./list/empresa.component";
import {EmpresaDetailComponent} from "./detail/empresa-detail.component";


const routes: Routes = [
  { path: "", component: EmpresaComponent},
  { path: "detail", component: EmpresaDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class EmpresaRoutingModule { }
