import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParametroRoutingModule} from "./parametro-routing.module";
import {ParametroComponent} from "./list/parametro.component";
import {ParametroDetailComponent} from "./parametro-detail/parametro-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ParametroService} from "../../../core/services/parametro.service";

@NgModule({
  imports: [CommonModule, ParametroRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule ],
  declarations: [ParametroComponent, ParametroDetailComponent],
  providers: [ParametroService]
})
export class ParametroModule { }
