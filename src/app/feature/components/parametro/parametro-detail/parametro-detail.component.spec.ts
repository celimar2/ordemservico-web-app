import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ParametroDetailComponent} from './parametro-detail.component';

describe('ParametroDetailComponent', () => {
  let component: ParametroDetailComponent;
  let fixture: ComponentFixture<ParametroDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametroDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
