import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ParametroComponent} from "./list/parametro.component";
import {ParametroDetailComponent} from "./parametro-detail/parametro-detail.component";


const routes: Routes = [
  { path: "", component: ParametroComponent},
  { path: "detail", component: ParametroDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class ParametroRoutingModule { }
