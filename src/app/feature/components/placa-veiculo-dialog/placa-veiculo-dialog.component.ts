import {Component, Inject, OnInit} from '@angular/core';
import {AtividadeEconomicaSelectDialogComponent} from "../fornecedor/cadastrar/atividade-economica-select-dialog.component";
import {ErrorStateMatcher, MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AtividadeEconomica} from "../../models/atividade-economica.model";
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {Globals} from "../../../shared/globals";


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}

@Component({
  selector: 'app-placa-veiculo-dialog',
  templateUrl: './placa-veiculo-dialog.component.html',
  styleUrls: ['./placa-veiculo-dialog.component.scss']
})
export class PlacaVeiculoDialogComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private globals: Globals,
    private dialogRef: MatDialogRef<AtividadeEconomicaSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AtividadeEconomica[]) {
  }

  // @Output() notify: EventEmitter<any> = new EventEmitter(true);

  dialogForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  ngOnInit() {
    this.dialogForm = this.fb.group({
      prefixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_PREFIXO_VALIDATOR)]],
      sufixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_SUFIXO_VALIDATOR)]],
    })
  }

  cancel(): void {
    this.dialogRef.close();
  }

  onSubmit(form) {
    const placa = form.get('prefixo').value.toUpperCase() + '-'+ form.get('sufixo').value;
    this.dialogRef.close(`${placa}`);
  }

}
