import {Component, OnInit} from '@angular/core';
import {Regiao} from "../../../models/regiao.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RegiaoService} from "../../../services/regiao.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-regiao-detail',
  templateUrl: './regiao-detail.component.html',
  styleUrls: ['./regiao-detail.component.scss']
})
export class RegiaoDetailComponent implements OnInit {

  constructor(private regiaoService: RegiaoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {

  }

  sub: any;
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  mode: string;
  regiaoId: string;
  regiao: Regiao;
  detailForm: FormGroup;

  filterName: string = "";

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.regiaoId = params['id'] || "0";
      });

    this.menuAccess = this.auth.getMenuAccess("Regiao");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;
    this.detailForm = this.formBuilder.group({
      id: [0, {disabled: true}],
      nome: ['', Validators.required]
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    if (this.mode != 'insert') {
      this.regiaoService.getById(this.regiaoId)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }

  onCancel() {
    this.router.navigate(['regiao']);
    return;
  }

  onSubmit() {
    if (this.mode == "insert" && this.menuAccess.create) {
      this.regiaoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['regiao']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update" && this.menuAccess.update) {
      this.regiaoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['regiao']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}
