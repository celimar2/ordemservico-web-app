import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegiaoDetailComponent} from './regiao-detail.component';

describe('RegiaoDetailComponent', () => {
  let component: RegiaoDetailComponent;
  let fixture: ComponentFixture<RegiaoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegiaoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegiaoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
