import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {Regiao} from "../../../models/regiao.model";
import {RegiaoService} from "../../../services/regiao.service";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-regiao',
  templateUrl: './regiao.component.html',
  styleUrls: ['./regiao.component.scss']
})
export class RegiaoComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private regiaoService: RegiaoService,
              public dialog: MatDialog) {

  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  regioes: Regiao[];
  displayedColumns = ['id', 'nome', 'actionsColumn'];
  menuAccess: MenuAccess;

  dataSource = new MatTableDataSource<Regiao>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.regiaoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    this.menuAccess  = this.auth.getMenuAccess("Regiao");
    // console.log(JSON.stringify(menuAccess));
    this.accessCreate = this.menuAccess.create;
    this.accessRead = this.menuAccess.read;
    this.accessUpdate = this.menuAccess.update;
    this.accessDelete = this.menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.regiaoService.getList()
      .subscribe(data => {
        this.regioes = data;
      });
  }

  delete(regiao: Regiao) {
    if (this.accessDelete) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent,
        {data: {title: "Excluir", message: "Confirma exclusão do registro?"}, width: '350px'}
      );

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.regiaoService.delete(regiao.id)
            .subscribe(data => {
              let idx = this.dataSource.data.findIndex(x => x.id == regiao.id);
              this.dataSource.data.splice(idx, 1);
              this.paginator._changePageSize(this.paginator.pageSize);
            })
        }
      });
    }
  }

  show(regiao: Regiao): void {
    if (this.accessRead) {
      this.router.navigate(['regiao/detail'], {queryParams: {mode: "read", id: regiao.id.toString()}});
    }
  }

  edit(regiao: Regiao): void {
    if (this.accessUpdate) {
      this.router.navigate(['regiao/detail'], {queryParams: {mode: "update", id: regiao.id.toString()}});
    }
  }

  add(): void {
    if (this.accessCreate) {
      this.router.navigate(['regiao/detail'], {queryParams: {mode: "insert"}});
      this.loadData();
    }
  }
}


