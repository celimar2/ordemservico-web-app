import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegiaoRoutingModule} from "../regiao/regiao-routing.module";
import {RegiaoComponent} from "./list/regiao.component";
import {RegiaoDetailComponent} from "../regiao/detail/regiao-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RegiaoService} from "../../services/regiao.service";
import {AuthService} from "../../../core/services/auth.service";

@NgModule({
  imports: [CommonModule, RegiaoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [RegiaoComponent, RegiaoDetailComponent],
  providers: [ RegiaoService, AuthService]
})
export class RegiaoModule { }
