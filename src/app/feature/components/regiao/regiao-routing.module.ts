import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {RegiaoComponent} from "./list/regiao.component";
import {RegiaoDetailComponent} from "../regiao/detail/regiao-detail.component";


const routes: Routes = [
  { path: "", component: RegiaoComponent},
  { path: "detail", component: RegiaoDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class RegiaoRoutingModule { }
