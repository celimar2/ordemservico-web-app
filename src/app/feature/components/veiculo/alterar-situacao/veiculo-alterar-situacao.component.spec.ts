import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VeiculoAlterarStatusComponent} from './veiculo-alterar-situacao.component';

describe('VeiculoAlterarStatusComponent', () => {
  let component: VeiculoAlterarStatusComponent;
  let fixture: ComponentFixture<VeiculoAlterarStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeiculoAlterarStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeiculoAlterarStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
