import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {VeiculoService} from "../../../services/veiculo.service";
import {Globals} from "../../../../shared/globals";
import {Location} from "@angular/common";
import * as moment from "moment";
import {EnumModel} from "../../../../core/models/enum.model";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-veiculo-alterar-situacao',
  templateUrl: './veiculo-alterar-situacao.component.html',
  styleUrls: ['./veiculo-alterar-situacao.component.scss']
})
export class VeiculoAlterarSituacaoComponent implements OnInit {

  constructor(private router: Router,
              private _location: Location,
              private fb: FormBuilder,
              private enumService: EnumsService,
              private veiculoService:  VeiculoService,
              private globals: Globals) {}

  detailForm: FormGroup;
  situacaoVeiculo: any = null;
  listaNovaSituacao: any[];
  situacoes: any[];
  situacaoAtual: EnumModel = null;
  placa: any = null;
  // matcher = new MyErrorStateMatcher();
  veiculo: any = null;
  str: String = "";
  public loading: boolean = false;

  ngOnInit() {
    this.veiculo = null;
    this.enumService.getSituacaoVeiculo()
      .subscribe(data => {
        this.situacoes = this.enumService.enumToArray(data);
      });

    this.detailForm = this.fb.group({
      prefixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_PREFIXO_VALIDATOR)]],
      sufixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_SUFIXO_VALIDATOR)]],
      situacao: ['', Validators.required],
    })
  }

  cancel(): void {
    this._location.back();
  }

  buscaVeiculo() {
    this.placa = this.detailForm.get('prefixo').value.toUpperCase() + '-' + this.detailForm.get('sufixo').value;
    this.loading = true;
    this.veiculoService.getByPlaca(this.placa)
      .subscribe(data => {
        this.loading = false;
        this.veiculo = data;
        if (this.veiculo != null) {
          this.listaNovaSituacao = this.situacoes.filter( x => x.key != this.veiculo.situacao );
          this.veiculo.dataSituacao = null; //moment(this.veiculo.dataSituacao).format("DD/MM/YYYY hh:mm:ss");
          this.situacaoAtual = this.situacoes.find( x => x.key == this.veiculo.situacao );
        }
      });
  }

  alterarSituacao() {
    this.loading = true;
    let veiculoAux = null;
    this.veiculo.situacao = this.detailForm.get('situacao').value;
    this.veiculo.dataSituacao = moment().format("YYYY-MM-DDThh:mm:ss");
    this.veiculoService.update(this.veiculo)
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          veiculoAux = data;
          if (veiculoAux.situacao == this.veiculo.situacao) {
            alert("Situação do veículo atualizada!");
            }
        },
        error => {
          this.loading = false;
          alert('Erro: '+error);
        },
        () => {
          this.loading = false;
          this.router.navigate(['veiculo']);
        }
      )
  }

}
