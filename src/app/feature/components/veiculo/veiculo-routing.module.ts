import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {VeiculoComponent} from "./list/veiculo.component";
import {VeiculoDetailComponent} from "./detail/veiculo-detail.component";
import {VeiculoCadastrarComponent} from "./cadastrar/veiculo-cadastrar.component";
import {VeiculoAlterarSituacaoComponent} from "./alterar-situacao/veiculo-alterar-situacao.component";

const routes: Routes = [
  { path: "", component: VeiculoComponent},
  { path: "detail", component: VeiculoDetailComponent},
  { path: "cadastrar", component: VeiculoCadastrarComponent},
  { path: "alterar-situacao", component: VeiculoAlterarSituacaoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculoRoutingModule { }
