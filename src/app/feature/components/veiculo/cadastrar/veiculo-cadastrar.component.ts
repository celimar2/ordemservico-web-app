import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Globals} from "../../../../shared/globals";
import {VeiculoService} from "../../../services/veiculo.service";
import {Veiculo} from "../../../models/veiculo.model";
import {Router} from "@angular/router";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {Location} from "@angular/common";

@Component({
  selector: 'app-veiculo-cadastrar',
  templateUrl: './veiculo-cadastrar.component.html',
  // styleUrls: ['./veiculo-cadastrar.component.scss']
})
export class VeiculoCadastrarComponent implements OnInit {

  constructor(private router: Router,
              private _location: Location,
              private fb: FormBuilder,
              private enumService: EnumsService,
              private veiculoService:  VeiculoService,
              private globals: Globals) {}

  // matcher = new MyErrorStateMatcher();
  veiculo: Veiculo = null;
  detailForm: FormGroup;
  situacaoVeiculo: any = null;
  situacoes: any[];

  ngOnInit() {

    this.veiculo = null;
    this.enumService.getSituacaoVeiculo()
      .subscribe(data => {
        this.situacoes = this.enumService.enumToArray(data);
      });
    this.detailForm = this.fb.group({
      prefixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_PREFIXO_VALIDATOR)]],
      sufixo: ['', [Validators.required, Validators.pattern(this.globals.PLACA_SUFIXO_VALIDATOR)]],
    })
  }

  cancel(): void {
    this._location.back();
  }

  submit(form) {
    const placa = this.detailForm.get('prefixo').value.toUpperCase()
                  + '-' +
                  this.detailForm.get('sufixo').value;

    this.veiculoService.getByPlaca(placa)
      .subscribe(data => {
        this.veiculo = data;
        if (this.veiculo != null) {
          this.situacaoVeiculo = this.situacoes.find( x => x.key == this.veiculo.situacao );
        } else {
          this.router.navigate(['veiculo/detail'],{ queryParams: { mode:"insert", placa:placa} });
        }
      });

  }

}
