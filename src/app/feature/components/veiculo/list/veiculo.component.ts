import {Component, OnInit, ViewChild} from '@angular/core';
import {VeiculoService} from "../../../services/veiculo.service";
import {Veiculo} from "../../../models/veiculo.model";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Router} from "@angular/router";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import {VeiculoListagem} from "../../../models/veiculo-listagem.model";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {EnumModel} from "../../../../core/models/enum.model";

@Component({
  selector: 'app-veiculo',
  templateUrl: './veiculo.component.html',
  styleUrls: ['./veiculo.component.scss']
})
export class VeiculoComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private veiculoService: VeiculoService,
              private enumService: EnumsService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  situacoesEnum: any[];
  situacoes: any[];
  veiculos: VeiculoListagem[];
  displayedColumns = ['id', 'placa', 'marca', 'modelo', 'cor', 'situacao', 'actionsColumn'];

  dataSource = new MatTableDataSource<VeiculoListagem>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Veiculo");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.enumService.getSituacaoVeiculo()
      .subscribe(data => {
        this.situacoes = this.enumService.enumToArray(data);
      });

    this.loadData();
  }

  ngAfterViewInit() {
    this.veiculoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  getSituacao(situacao: EnumModel): string {
    let sit: EnumModel = null;
    sit = this.situacoes.find(v => v.key == situacao);
    return sit.value.replace("_"," ");
  }

  loadData() {
    this.veiculoService.getList()
      .subscribe(data => {
        this.veiculos = data;
      });
  }

  delete(veiculo: Veiculo) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"}, width: '350px'}
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.veiculoService.delete(veiculo.id)
          .subscribe(data => {
            let idx = this.dataSource.data.findIndex( x => x.id == veiculo.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(veiculo: Veiculo): void {
    if (this.accessRead) {
      this.router.navigate(['veiculo/detail'], {queryParams: {mode: "read", id: veiculo.id}});
    }
  };

  edit(veiculo: Veiculo): void {
    if (this.accessUpdate) {
      this.router.navigate(['veiculo/detail'], {queryParams: {mode: "update", id: veiculo.id}});
    }
  };

  add(): void {
    this.router.navigate(['veiculo/cadastrar']);
  }

}
