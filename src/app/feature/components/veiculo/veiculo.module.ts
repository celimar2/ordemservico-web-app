import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VeiculoRoutingModule} from "../veiculo/veiculo-routing.module";
import {VeiculoComponent} from "./list/veiculo.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {VeiculoService} from "../../services/veiculo.service";
import {VeiculoDetailComponent} from "./detail/veiculo-detail.component";
import {VeiculoCadastrarComponent} from "./cadastrar/veiculo-cadastrar.component";
import {VeiculoAlterarSituacaoComponent} from "./alterar-situacao/veiculo-alterar-situacao.component";

@NgModule({
  imports: [CommonModule, VeiculoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [VeiculoComponent, VeiculoDetailComponent, VeiculoCadastrarComponent, VeiculoAlterarSituacaoComponent],
  providers: [ VeiculoService ]
})
export class VeiculoModule { }
