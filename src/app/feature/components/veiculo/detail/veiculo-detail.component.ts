import {Component, OnInit} from '@angular/core';
import {MarcaVeiculoService} from "../../../services/marca-veiculo.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";
import {MarcaVeiculo} from "../../../models/marca-veiculo.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Veiculo} from "../../../models/veiculo.model";
import {VeiculoService} from "../../../services/veiculo.service";
import {Globals} from "../../../../shared/globals";
import {EnumsService} from "../../../../core/services/enums.service.ts";
import * as moment from "moment";
import {EnumModel} from "../../../../core/models/enum.model";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-veiculo-detail',
  templateUrl: './veiculo-detail.component.html',
  styleUrls: ['./veiculo-detail.component.scss']
})
export class VeiculoDetailComponent implements OnInit {

  constructor(private regiaoService: MarcaVeiculoService,
              private veiculoService: VeiculoService,
              private enumService: EnumsService,
              private formBuilder: FormBuilder,
              private globals: Globals,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  id: number;
  veiculo: Veiculo;
  marcas: MarcaVeiculo[];
  detailForm: FormGroup;
  ultimaSituacao: any;
  placa: string;
  ano: number = 0;

  public anoMask: any;
  public placaMask: any;
  public situacoes: any[];

  ngOnInit() {

    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.id = params['id'] || "";
        this.placa = params['placa'] || "";
      });

    this.menuAccess = this.auth.getMenuAccess("Veiculo");

    //console.log(JSON.stringify(this.menuAccess));
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) ||
      (this.mode == "update" && this.menuAccess.update);

    this.placaMask = this.globals.PLACA_MASK;
    this.anoMask = this.globals.ANO_MASK;

    this.enumService.getSituacaoVeiculo()
      .subscribe(data => {
        this.situacoes = this.enumService.enumToArray(data)
          .sort((a: EnumModel, b: EnumModel) => a.key > b.key ? 1 : a.key < b.key ? -1 : 0);
      });

    this.ano = Number.parseInt(moment().format('YYYY'));

    this.detailForm = this.formBuilder.group({
      id: 0,
      placa: [this.placa, [Validators.required, Validators.pattern(this.globals.PLACA_VALIDATOR)]],
      marca: [null, Validators.required],
      modelo: ['', Validators.required],
      cor: ['', Validators.required],
      anoFabricacao: [this.ano, [Validators.required, Validators.pattern(this.globals.ANO_VALIDATOR)]],
      anoModelo: [this.ano, [Validators.required, Validators.pattern(this.globals.ANO_VALIDATOR)]],
      origem: '',
      entrada: moment().format(this.globals.DATETIME_FMT),
      saida: null,
      situacao: ['1-AVOPEN', Validators.required],
      dataSituacao: [moment().format(this.globals.DATETIME_FMT)],
      notas: ''
    });

    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    this.regiaoService.getList()
      .subscribe(data => {
          this.marcas = data
        }
      );

    this.ultimaSituacao = this.detailForm.get('dataSituacao').value;

    if (this.mode != "insert") {
      this.veiculoService.getById(this.id)
        .subscribe(data => {
          this.detailForm.setValue(data);
          this.ultimaSituacao = this.detailForm.get('situacao').value;
        });
    }
  }

  statusChange(situacao) {
    if (situacao != this.ultimaSituacao) {
      this.detailForm.get('dataSituacao').setValue(moment().format("YYYY-MM-DD hh:mm:ss"));
    }
  }

  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

  formatData(formData) {
    this.detailForm.get(formData)
      .setValue(moment(this.detailForm.get(formData).value)
        .format(this.globals.DATETIME_FMT)
      );
  }

  onSubmit() {
    this.formatData('dataSituacao');
    this.formatData('entrada');

    if (this.detailForm.get('saida').value != null) {
      this.formatData('saida');
    }
    this.detailForm.get('modelo').setValue(this.detailForm.get('modelo').value.toUpperCase());
    this.detailForm.get('cor').setValue(this.detailForm.get('cor').value.toUpperCase());
    if (this.mode == "insert") {
      this.veiculoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['veiculo']);
            return;
          },
          error => {
            alert('Erro: ' + error);
          });

    } else if (this.mode == "update") {
      this.veiculoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['veiculo']);
            return;
          },
          error => {
            alert('Erro: ' + error);
          });
    }
  }
}
