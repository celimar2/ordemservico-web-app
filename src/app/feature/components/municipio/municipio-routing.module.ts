import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {MunicipioComponent} from "./list/municipio.component";
import {MunicipioDetailComponent} from "./detail/municipio-detail.component";


const routes: Routes = [
  { path: "", component: MunicipioComponent},
  { path: "detail", component: MunicipioDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class MunicipioRoutingModule { }
