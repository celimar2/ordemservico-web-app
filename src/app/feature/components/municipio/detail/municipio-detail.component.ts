import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MunicipioService} from "../../../services/municipio.service";
import {first} from "rxjs/operators";
import {Municipio} from "../../../models/municipio.model";
import {EstadoService} from "../../../services/estado.service";
import {Estado} from "../../../models/estado.model";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-municipio-detail',
  templateUrl: './municipio-detail.component.html',
  styleUrls: ['./municipio-detail.component.scss']
})
export class MunicipioDetailComponent implements OnInit {

  constructor(private municipioService: MunicipioService,
              private estadoService: EstadoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  municipioId: string;
  municipio: Municipio;
  estados: Estado[];
  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route.queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.municipioId = params['id'] || "0";
      });
    this.menuAccess = this.auth.getMenuAccess("Municipio");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;

    this.detailForm = this.formBuilder.group({
      id: [0,{ disabled: true}],
      nome: ['', Validators.required],
      codigoIbge: [0, Validators.required],
      estado: [null, Validators.required]
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    this.estadoService.getList()
      .subscribe( data => {
        this.estados = data;
      });

    if (this.mode != 'insert') {
      this.municipioService.getById(this.municipioId)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }

  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

  onCancel() {
    this.router.navigate(['municipio']);
    return;
  }

  onSubmit() {
    if (this.mode == "insert") {
      this.municipioService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['municipio']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.municipioService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['municipio']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }

}


