import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MunicipioRoutingModule} from "../municipio/municipio-routing.module";
import {MunicipioComponent} from "./list/municipio.component";
import {MunicipioDetailComponent} from "../municipio/detail/municipio-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MunicipioService} from "../../services/municipio.service";

@NgModule({
  imports: [CommonModule, MunicipioRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [MunicipioComponent, MunicipioDetailComponent],
  providers: [ MunicipioService ]
})
export class MunicipioModule { }
