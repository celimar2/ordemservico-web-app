import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {MunicipioService} from "../../../services/municipio.service";
import {Municipio} from "../../../models/municipio.model";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-municipio',
  templateUrl: './municipio.component.html',
  styleUrls: ['./municipio.component.scss']
})
export class MunicipioComponent implements OnInit {

  constructor(private router: Router,
              private auth: AuthService,
              private municipioService: MunicipioService,
              public dialog: MatDialog) {
  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  municipios: Municipio[];
  displayedColumns = [ 'id', 'nome', 'codigoIbge', 'uf', 'actionsColumn'];
  dataSource = new MatTableDataSource<Municipio>();

  public loading = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {

    this.loading = true;

    this.municipioService.getList().subscribe(
      data => {this.dataSource.data = data;}
      ,() => {this.loading = false;}
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Municipio");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.municipioService.getList()
      .subscribe( data => {
        this.municipios = data;
      });
  }

  delete(municipio: Municipio){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.municipioService.delete(municipio.id)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.id == municipio.id);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(municipio: Municipio): void {
    if (this.accessRead) {
      this.router.navigate(['municipio/detail'], {queryParams: {mode: "read", id: municipio.id}});
      this.loadData();
    }
  };

  edit(municipio: Municipio): void {
    if (this.accessUpdate) {
      this.router.navigate(['municipio/detail'], {queryParams: {mode: "update", id: municipio.id}});
      this.loadData();
    }
  };

  add(): void {
    this.router.navigate(['municipio/detail'],{ queryParams: { mode:"insert" }});
    this.loadData();
  };

}

