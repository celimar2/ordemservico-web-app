import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Estado} from "../../../models/estado.model";
import {EstadoService} from "../../../services/estado.service";
import {ConfirmDialogComponent} from "../../../../shared/dialogs/confirm-dialog.component";
import {AuthService} from "../../../../core/services/auth.service";
import {MenuAccess} from "../../../../core/models/menu-access.model";

@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.scss']
})
export class EstadoComponent implements OnInit {

  constructor(private estadoService: EstadoService,
              private auth: AuthService,
              private router: Router,
              public dialog: MatDialog) {

  }

  accessCreate: boolean = false;
  accessRead: boolean = false;
  accessUpdate: boolean = false;
  accessDelete: boolean = false;
  estados: Estado[];
  displayedColumns = [ 'uf', 'nome', 'codigoIbge', 'regiao', 'actionsColumn'];
  dataSource = new MatTableDataSource<Estado>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.estadoService.getList().subscribe(data => {
      this.dataSource.data = data;
    });
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let menuAccess: MenuAccess = this.auth.getMenuAccess("Estado");
    this.accessCreate = menuAccess.create;
    this.accessRead = menuAccess.read;
    this.accessUpdate = menuAccess.update;
    this.accessDelete = menuAccess.delete;
    this.loadData();
  }

  loadData() {
    this.estadoService.getList()
      .subscribe( data => {
        this.estados = data;
      });
  }

  delete(estado: Estado){

    const dialogRef = this.dialog.open( ConfirmDialogComponent,
      {data: {title: "Excluir", message: "Confirma exclusão do registro?"},width : '350px' }
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.estadoService.delete(estado.uf)
          .subscribe( data => {
            let idx = this.dataSource.data.findIndex( x => x.uf == estado.uf);
            this.dataSource.data.splice(idx,1);
            this.paginator._changePageSize(this.paginator.pageSize);
          })
      }
    });

  };

  show(estado: Estado): void {
    if (this.accessRead) {
      this.router.navigate(['estado/detail'],{ queryParams: { mode:"read", uf:estado.uf} });
      this.loadData();
    }
  }

  edit(estado: Estado): void {
    if (this.accessUpdate) {
      this.router.navigate(['estado/detail'],{ queryParams: { mode:"update", uf:estado.uf} });
      this.loadData();
    }
  }

  add(): void {
    this.router.navigate(['estado/detail'],{ queryParams: { mode:"insert" }});
    this.loadData();
  };

}

