import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {EstadoComponent} from "./list/estado.component";
import {EstadoDetailComponent} from "./detail/estado-detail.component";


const routes: Routes = [
  { path: "", component: EstadoComponent},
  { path: "detail", component: EstadoDetailComponent},
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class EstadoRoutingModule { }
