import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EstadoRoutingModule} from "./estado-routing.module";
import {EstadoComponent} from "./list/estado.component";
import {EstadoDetailComponent} from "./detail/estado-detail.component";
import {CustomGuiModule} from "../../../shared/custom-gui.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {EstadoService} from "../../services/estado.service";

@NgModule({
  imports: [CommonModule, EstadoRoutingModule, CustomGuiModule, FormsModule, ReactiveFormsModule],
  declarations: [EstadoComponent, EstadoDetailComponent],
  providers: [ EstadoService ]
})
export class EstadoModule { }
