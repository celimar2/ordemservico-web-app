import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {EstadoService} from "../../../services/estado.service";
import {first} from "rxjs/operators";
import {Estado} from "../../../models/estado.model";
import {RegiaoService} from "../../../services/regiao.service";
import {Regiao} from "../../../models/regiao.model";
import {MenuAccess} from "../../../../core/models/menu-access.model";
import {AuthService} from "../../../../core/services/auth.service";

@Component({
  selector: 'app-estado-detail',
  templateUrl: './estado-detail.component.html',
  styleUrls: ['./estado-detail.component.scss']
})
export class EstadoDetailComponent implements OnInit {

  constructor(private regiaoService: RegiaoService,
              private estadoService: EstadoService,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }
  menuAccess: MenuAccess = null;
  saveEnabled: boolean = false;
  sub: any;
  mode: string;
  estadoUf: string;
  estado: Estado;
  regioes: Regiao[];


  detailForm: FormGroup;

  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.mode = params['mode'] || "";
        this.estadoUf = params['uf' ] || "";
      });
    this.menuAccess = this.auth.getMenuAccess("Estado");
    this.saveEnabled = (this.mode == "insert" && this.menuAccess.create) || (this.mode == "update" && this.menuAccess.update) ;


    this.detailForm = this.formBuilder.group({
      uf: ['',Validators.required],
      nome: ['', Validators.required],
      codigoIbge: [0, Validators.required],
      regiao: [null, Validators.required]
    });
    if (!this.saveEnabled) {
      for (let control in this.detailForm.controls) {
        this.detailForm.controls[control].disable();
      }
    }

    this.regiaoService.getList()
      .subscribe(data => {
        this.regioes = data}
      );

    if(this.mode != "insert") {
      this.estadoService.getByUf(this.estadoUf)
        .subscribe( data => {
          this.detailForm.setValue(data);
        });
    }
  }

  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.nome === f2.nome;
  }

  onCancel() {
    this.router.navigate(['estado']);
    return;
  }

  onSubmit() {
    if (this.mode == "insert") {
      this.estadoService.create(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['estado']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });

    } else if (this.mode == "update") {
      this.estadoService.update(this.detailForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(['estado']);
            return;
          },
          error => {
            alert('Erro: '+error);
          });
    }
  }
}
