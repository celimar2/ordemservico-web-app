import {AtividadeEconomica} from "./atividade-economica.model";
import {Telefone} from "./telefone.model";
import {Municipio} from "./municipio.model";
import {Estado} from "./estado.model";

export interface Filial {

  id: number;
  cnpj: string;
  razaoSocial: string;
  nomeFantasia: string;
  inscricaoEstadual: string;
  inscricaoMunicipal: string;
  abertura: Date;
  chaveSistema: string;
  ativa: boolean;
  notas: string;
  atividadesEconomicas: AtividadeEconomica[];
  cep: string;
  logradouro: string;
  complemento: string;
  bairro: string;
  estado: Estado;
  municipio: Municipio;
  telefones: Telefone[];

}
