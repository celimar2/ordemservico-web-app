import {Estado} from "./estado.model";
import {Municipio} from "./municipio.model";
import {Telefone} from "./telefone.model";
import {AtividadeEconomica} from "./atividade-economica.model";

export interface Fornecedor {

  id: number;
  tipoJuridico: string;
  cpfCnpj: string;
  nomeRazaoSocial: string;
  apelidoNomeFantasia: string;
  genero: string;
  identidadeInscricaoEstadual: string;
  nascimentoAbertura: Date;
  email: string;
  ativo: boolean;
  notas: string;
  atividadesEconomicas: AtividadeEconomica[];
  cep: string;
  logradouro: string;
  complemento: string;
  bairro: string;
  estado: Estado;
  municipio: Municipio;
  telefones: Telefone[];
}
