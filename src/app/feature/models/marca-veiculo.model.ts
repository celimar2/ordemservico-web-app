
export interface MarcaVeiculo {

  id: number;
  nome: string;
  sigla: string;
}
