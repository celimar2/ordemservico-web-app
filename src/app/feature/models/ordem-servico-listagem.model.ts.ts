
export interface OrdemServicoListagem {
  id: number;
  veiculo: string;
  fornecedor: string;
  situacao: string ;
  retiradaDataHora: Date;
  devolucaoDataHora: Date;
}
