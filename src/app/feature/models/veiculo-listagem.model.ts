

export interface VeiculoListagem {

  id: number;
  placa: string;
  marca: string;
  modelo:string
  cor: string;
  situacao: string;
  dataSituacao: Date;
  entrada: Date;
  saida: Date;
}
