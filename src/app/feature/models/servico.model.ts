import {MarcaVeiculo} from "./marca-veiculo.model";

export interface Veiculo {

  id: number;
  placa: string;
  marca: MarcaVeiculo;
  modelo:string
  cor: string;
  ano_fabricacao: number;
  ano_modelo: number;
  entrada: Date;
  origem: string;
  saida: Date;
  situacao: string;
  dataSituacao: Date;
  notas: string;
}
