import {Regiao} from "./regiao.model";

export interface Estado {

  uf: string;
  nome: string;
  codigoIbge: number;
  regiao: Regiao;
}
