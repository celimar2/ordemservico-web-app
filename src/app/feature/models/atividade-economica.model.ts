
export interface AtividadeEconomica {
  id: number;
  nome: string;
  ativo: boolean;
}
