import {TinyOrdemServico} from "./tiny-ordem-servico.model";

export interface OsListByFornecedor {
  id: number;
  apelido: string;
  ordens: TinyOrdemServico[];
}
