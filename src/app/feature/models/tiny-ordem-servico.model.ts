
export interface TinyOrdemServico {
  id: number;
  descricao: string;
  abertura: Date;
  previsao: Date;
  devolucao: Date;
  natureza: string;
  situacao: string;
  veiculoId: number;
  placa: string;
  marca: string;
  modelo: string;
  cor: string;
}
