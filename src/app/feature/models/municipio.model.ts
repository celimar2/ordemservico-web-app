// import {Estado} from "./estado.model";

export interface Municipio {

  id: number;
  nome: string;
  codigoIbge: number;
  uf: string;
  // estado: Estado;
}
