import {MarcaVeiculo} from "./marca-veiculo.model";

export interface Veiculo {

  id: number;
  placa: string;
  marca: MarcaVeiculo;
  modelo:string
  cor: string;
  origem: string;
  anoFabricacao: number;
  anoModelo: number;
  entrada: Date;
  saida: Date;
  situacao: string;
  dataSituacao: Date;
  notas: string;
}
