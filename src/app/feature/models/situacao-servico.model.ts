
export interface SituacaoServico {

  id: number;
  nome: string;
  sequencia: number;
  descricao: string;
  ativo: boolean;
}
