import {Estado} from "./estado.model";
import {Municipio} from "./municipio.model";
import {Telefone} from "./telefone.model";
import {User} from "../../core/models/user.model";
import {Filial} from "./filial.model";

export interface Colaborador {
  id: number;
  nome: string;
  apelido: string;
  cpf: string;
  identidadeNumero: string;
  identidadeOrgaoExpedidor: string ;
  nascimento: Date;
  genero: string;
  email: string;
  ativo: Boolean;
  notas: string;
  user: User;
  cep: string;
  logradouro: string;
  complemento: string;
  bairro: string;
  estado: Estado;
  municipio: Municipio;
  telefones: Telefone[];
  filial: Filial[];
}


