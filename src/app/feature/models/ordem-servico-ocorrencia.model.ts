
export interface OrdemServicoOcorrencia {
  id: number;
  // ordemServicoId: number;
  data: Date;
  observacao: string;
  valor: number;
}
