
export interface NaturezaServico {

  id: number;
  nome: string;
  obervacao: string;
  ativo: boolean;
}
