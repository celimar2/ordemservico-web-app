import {Fornecedor} from "./fornecedor.model";
import {Veiculo} from "./veiculo.model";
import {OrdemServicoOcorrencia} from "./ordem-servico-ocorrencia.model";
import {NaturezaServico} from "./natureza-servico.model";
import {SituacaoServico} from "./situacao-servico.model";

export interface OrdemServico {
  id: number;
  veiculo: Veiculo;
  descricao: string;
  abertura: Date;
  naturezaServico: NaturezaServico;
  devolucaoPrevisao: Date;
  fornecedor: Fornecedor;

  // filial: Filial ;
  situacaoServico: SituacaoServico;

  numeroOsFornecedore: string ;
  retiradaDataHora: Date;
  retiradaValorPrevisto: number;
  retiradaObservacoes: string ;
  retiradaResponsavelColaborador: string;
  retiradaResponsavelFornecedor: string ;

  devolucaoDataHora: Date;
  devolucaoValorRealizado:number;
  devolucaoObservacoes: string ;
  devolucaoResponsavelColaborador: string ;
  devolucaoResponsavelFornecedor: string ;
  ocorrencias: OrdemServicoOcorrencia[];
}
