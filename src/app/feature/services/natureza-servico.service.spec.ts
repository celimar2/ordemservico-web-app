import {inject, TestBed} from '@angular/core/testing';

import {NaturezaServicoService} from './natureza-servico.service';

describe('NaturezaServicoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NaturezaServicoService]
    });
  });

  it('should be created', inject([NaturezaServicoService], (service: NaturezaServicoService) => {
    expect(service).toBeTruthy();
  }));
});
