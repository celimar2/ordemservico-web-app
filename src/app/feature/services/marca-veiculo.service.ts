import {Injectable} from '@angular/core';
import {MarcaVeiculo} from "../models/marca-veiculo.model";
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MarcaVeiculoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private marcaVeiculoUrl = this.globals.HOSTNAME_PORT + '/api/marca-veiculos';


  public getList() {
    return this.http.get<MarcaVeiculo[]>(this.marcaVeiculoUrl);
  }

  public getById(id) {
    return this.http.get<MarcaVeiculo>(this.marcaVeiculoUrl + "/"+id);
  }

  public update(marcaVeiculo) {
    return this.http.put(this.marcaVeiculoUrl, marcaVeiculo);
  }

  public create(marcaVeiculo) {
    return this.http.post<MarcaVeiculo>(this.marcaVeiculoUrl, marcaVeiculo);
  }

  public delete(id) {
    return this.http.delete(this.marcaVeiculoUrl + "/"+ id);
  }

}
