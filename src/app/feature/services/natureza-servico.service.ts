import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {NaturezaServico} from "../models/natureza-servico.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class NaturezaServicoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private naturezaServicoUrl = this.globals.HOSTNAME_PORT + '/api/natureza-servicos';


  public getList() {
    return this.http.get<NaturezaServico[]>(this.naturezaServicoUrl);
  }

  public getById(id) {
    return this.http.get<NaturezaServico>(this.naturezaServicoUrl + "/"+id);
  }

  public update(naturezaServico) {
    return this.http.put(this.naturezaServicoUrl, naturezaServico);
  }

  public create(naturezaServico) {
    return this.http.post<NaturezaServico>(this.naturezaServicoUrl, naturezaServico);
  }

  public delete(id) {
    return this.http.delete(this.naturezaServicoUrl + "/"+ id);
  }

}
