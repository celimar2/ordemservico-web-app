import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Globals} from "../../shared/globals";
import {OrdemServico} from "../models/ordem-servico.model";
import {OsListByFornecedor} from "../models/ordem-servico-list.model";
import {OrdemServicoListagem} from "../models/ordem-servico-listagem.model.ts";

@Injectable({
  providedIn: 'root'
})
export class OrdemServicoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private ordemServicoUrl = this.globals.HOSTNAME_PORT + '/api/ordens-servico';

  public getAll() {
    return this.http.get<OrdemServico[]>(this.ordemServicoUrl);
  }

  public getList() {
    return this.http.get<OrdemServicoListagem[]>(this.ordemServicoUrl+"/listagem");
  }

  public getListByFornecedor() {
    return this.http.get<OsListByFornecedor[]>(this.ordemServicoUrl +'/byFornecedor');
  }

  public getById(id) {
    return this.http.get<OrdemServico>(this.ordemServicoUrl + "/"+id);
  }

  public update(ordemServico) {
    return this.http.put(this.ordemServicoUrl, ordemServico);
  }

  public create(ordemServico) {
    return this.http.post<OrdemServico>(this.ordemServicoUrl, ordemServico);
  }

  public delete(id) {
    return this.http.delete(this.ordemServicoUrl + "/"+ id);
  }

}
