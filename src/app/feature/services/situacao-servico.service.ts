import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {SituacaoServico} from "../models/situacao-servico.model";

@Injectable({
  providedIn: 'root'
})
export class SituacaoServicoService {


  constructor(private http:HttpClient, private globals:Globals) {}

  private situacaoServicoUrl = this.globals.HOSTNAME_PORT + '/api/situacao-servicos';


  public getList() {
    return this.http.get<SituacaoServico[]>(this.situacaoServicoUrl);
  }

  public getById(id) {
    return this.http.get<SituacaoServico>(this.situacaoServicoUrl + "/"+id);
  }

  public update(situacaoServico) {
    return this.http.put(this.situacaoServicoUrl, situacaoServico);
  }

  public create(situacaoServico) {
    return this.http.post<SituacaoServico>(this.situacaoServicoUrl, situacaoServico);
  }

  public delete(id) {
    return this.http.delete(this.situacaoServicoUrl + "/"+ id);
  }

  // search(term) {
  //   var listOfBooks = this.httpService.get('https://localhost:25252/api/books/' + term)
  //     .pipe(
  //       debounceTime(500),  // WAIT FOR 500 MILISECONDS ATER EACH KEY STROKE.
  //       map(
  //         (data: any) => {
  //           return (
  //             data.length != 0 ? data as any[] : [{"BookName": "No Record Found"} as any]
  //           );
  //         }
  //       ));
  //
  //   return listOfBooks;
  // }

}
