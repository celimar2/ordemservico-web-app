import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {AtividadeEconomica} from "../models/atividade-economica.model";

@Injectable({
  providedIn: 'root'
})
export class AtividadeEconomicaService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private atividadeEconomicaUrl = this.globals.HOSTNAME_PORT + '/api/atividades-economicas';


  public getList() {
    return this.http.get<AtividadeEconomica[]>(this.atividadeEconomicaUrl);
  }

  public getListAtivos() {
    return this.http.get<AtividadeEconomica[]>(this.atividadeEconomicaUrl+ "/ativos");
  }

  public getById(id) {
    return this.http.get<AtividadeEconomica>(this.atividadeEconomicaUrl + "/"+id);
  }

  public update(atividadeEconomica) {
    return this.http.put(this.atividadeEconomicaUrl, atividadeEconomica);
  }

  public create(atividadeEconomica) {
    return this.http.post<AtividadeEconomica>(this.atividadeEconomicaUrl, atividadeEconomica);
  }

  public delete(id) {
    return this.http.delete(this.atividadeEconomicaUrl + "/"+ id);
  }

}
