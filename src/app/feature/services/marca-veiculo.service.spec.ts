import {inject, TestBed} from '@angular/core/testing';

import {MarcaVeiculoService} from './marca-veiculo.service';

describe('MarcaVeiculoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarcaVeiculoService]
    });
  });

  it('should be created', inject([MarcaVeiculoService], (service: MarcaVeiculoService) => {
    expect(service).toBeTruthy();
  }));
});
