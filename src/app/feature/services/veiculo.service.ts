import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Globals} from "../../shared/globals";
import {Veiculo} from "../models/veiculo.model";
import {VeiculoListagem} from "../models/veiculo-listagem.model";
import {Totais} from "../models/Totais";

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private veiculoUrl = this.globals.HOSTNAME_PORT + '/api/veiculos';


  public getAll() {
    return this.http.get<Veiculo[]>(this.veiculoUrl);
  }

  public getList() {
    return this.http.get<VeiculoListagem[]>(this.veiculoUrl+"/list");
  }

  public getTotalsWithParams(from: String, to: String) {
    let parameters: string = '';
    parameters = parameters + '?from='+from;
    if (to != null) {
      parameters = parameters + '&to='+to;
    }
    return this.http.get<Totais[]>(this.veiculoUrl+"/totais"+parameters);
  }

  public getListWithParams(from: String, to: String, situacoes: any[]) {
    let parameters: string = '';
    parameters = parameters + '?from='+from;
    if (to != null) {
      parameters = parameters + '&to='+to;
    }
    situacoes.forEach(value => {
      parameters = parameters + "&situacoes="+value;
    })
  return this.http.get<VeiculoListagem[]>(this.veiculoUrl+"/listagem"+parameters);
  }

  public getById(id) {
    return this.http.get<Veiculo>(this.veiculoUrl + "/"+id);
  }

  public changeStituacao(veiculo) {
    return this.http.put<Veiculo>(this.veiculoUrl+"/alterar-situacao", veiculo);
  }

  public getByPlaca(placa) {
    return this.http.get<Veiculo>(this.veiculoUrl + "/placa/"+placa);
  }

  public create(veiculo) {
    return this.http.post<Veiculo>(this.veiculoUrl, veiculo);
  }

  public update(veiculo) {
    return this.http.put(this.veiculoUrl, veiculo);
  }

  public delete(id) {
    return this.http.delete(this.veiculoUrl + "/"+ id);
  }

}
