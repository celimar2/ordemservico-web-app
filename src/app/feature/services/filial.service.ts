import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {Filial} from "../models/filial.model";

@Injectable({
  providedIn: 'root'
})
export class FilialService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private filialUrl = this.globals.HOSTNAME_PORT + '/api/filiais';


  public getList() {
    return this.http.get<Filial[]>(this.filialUrl);
  }

  public getById(id) {
    return this.http.get<Filial>(this.filialUrl + "/"+id);
  }

  public update(filial) {
    return this.http.put(this.filialUrl, filial);
  }

  public create(filial) {
    return this.http.post<Filial>(this.filialUrl, filial);
  }

  public delete(id) {
    return this.http.delete(this.filialUrl + "/"+ id);
  }

}
