import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Globals} from "../../shared/globals";
import {Colaborador} from "../models/colaborador.model";

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private colaboradorUrl = this.globals.HOSTNAME_PORT + '/api/colaboradores';


  public getList() {
    return this.http.get<Colaborador[]>(this.colaboradorUrl);
  }

  public getById(id) {
    return this.http.get<Colaborador>(this.colaboradorUrl + "/"+id);
  }

  public getByCpf(cpf) {
    return this.http.put<Colaborador>(this.colaboradorUrl + "/cpf",cpf);
  }

  public update(colaborador) {
    return this.http.put(this.colaboradorUrl, colaborador);
  }

  public create(colaborador) {
    return this.http.post<Colaborador>(this.colaboradorUrl, colaborador);
  }

  public delete(id) {
    return this.http.delete(this.colaboradorUrl + "/"+ id);
  }

}
