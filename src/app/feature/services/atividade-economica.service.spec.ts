import {inject, TestBed} from '@angular/core/testing';

import {AtividadeEconomicaService} from './atividade-economica.service';

describe('AtividadeEconomicaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AtividadeEconomicaService]
    });
  });

  it('should be created', inject([AtividadeEconomicaService], (service: AtividadeEconomicaService) => {
    expect(service).toBeTruthy();
  }));
});
