import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {Parametro} from "../models/parametro.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ParametroService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private parametroUrl = this.globals.HOSTNAME_PORT + '/api/parametros';


  public getList() {
    return this.http.get<Parametro[]>(this.parametroUrl);
  }

  public getById(id) {
    return this.http.get<Parametro>(this.parametroUrl + "/"+id);
  }

  public update(parametro) {
    return this.http.put(this.parametroUrl, parametro);
  }

  public create(parametro) {
    return this.http.post<Parametro>(this.parametroUrl, parametro);
  }

  public delete(id) {
    return this.http.delete(this.parametroUrl + "/"+ id);
  }

}
