import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {Fornecedor} from "../models/fornecedor.model";

@Injectable({
  providedIn: 'root'
})
export class FornecedorService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private fornecedorUrl = this.globals.HOSTNAME_PORT + '/api/fornecedores';


  public getList() {
    return this.http.get<Fornecedor[]>(this.fornecedorUrl);
  }

  public getById(id) {
    return this.http.get<Fornecedor>(this.fornecedorUrl + "/"+id);
  }

  public getByCpfCnpj(cpfCnpjRequest) {
    return this.http.put<Fornecedor>(this.fornecedorUrl + "/cpf-cnpj",cpfCnpjRequest);
  }

  public create(fornecedor) {
    return this.http.post<Fornecedor>(this.fornecedorUrl, fornecedor);
  }

  public update(fornecedor) {
    return this.http.put(this.fornecedorUrl, fornecedor);
  }

  public delete(id) {
    return this.http.delete(this.fornecedorUrl + "/"+ id);
  }

}
