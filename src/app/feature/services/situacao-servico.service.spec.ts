import {inject, TestBed} from '@angular/core/testing';

import {SituacaoServicoService} from './situacao-servico.service';

describe('SituacaoServicoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SituacaoServicoService]
    });
  });

  it('should be created', inject([SituacaoServicoService], (service: SituacaoServicoService) => {
    expect(service).toBeTruthy();
  }));
});
