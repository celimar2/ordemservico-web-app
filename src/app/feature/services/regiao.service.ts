import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Globals} from "../../shared/globals";
import {Regiao} from "../models/regiao.model";

@Injectable({
  providedIn: 'root'
})
export class RegiaoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private regiaoUrl = this.globals.HOSTNAME_PORT + '/api/regioes';


  public getList() {
    return this.http.get<Regiao[]>(this.regiaoUrl);
  }

  public getById(id) {
    return this.http.get<Regiao>(this.regiaoUrl + "/"+id);
  }

  public update(regiao) {
    return this.http.put(this.regiaoUrl, regiao);
  }

  public create(regiao) {
    return this.http.post<Regiao>(this.regiaoUrl, regiao);
  }

  public delete(id) {
    return this.http.delete(this.regiaoUrl + "/"+ id);
  }

}
