import {inject, TestBed} from '@angular/core/testing';

import {RegiaoService} from './regiao.service';

describe('RegiaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegiaoService]
    });
  });

  it('should be created', inject([RegiaoService], (service: RegiaoService) => {
    expect(service).toBeTruthy();
  }));
});
