import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {Municipio} from "../models/municipio.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {
  constructor(private http:HttpClient, private globals:Globals) {}

  private municipioUrl = this.globals.HOSTNAME_PORT + '/api/municipios';


  public getList() {
    return this.http.get<Municipio[]>(this.municipioUrl);
  }

  public getById(id) {
    return this.http.get<Municipio>(this.municipioUrl + "/"+id);
  }

  public update(municipio) {
    return this.http.put(this.municipioUrl, municipio);
  }

  public create(municipio) {
    return this.http.post<Municipio>(this.municipioUrl, municipio);
  }

  public delete(id) {
    return this.http.delete(this.municipioUrl + "/"+ id);
  }

}
