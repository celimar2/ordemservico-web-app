import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {Empresa} from "../models/empresa.model";

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private empresaUrl = this.globals.HOSTNAME_PORT + '/api/empresas';


  public getList() {
    return this.http.get<Empresa[]>(this.empresaUrl);
  }

  public getById(id) {
    return this.http.get<Empresa>(this.empresaUrl + "/"+id);
  }

  public update(empresa) {
    return this.http.put(this.empresaUrl, empresa);
  }

  public create(empresa) {
    return this.http.post<Empresa>(this.empresaUrl, empresa);
  }

  public delete(id) {
    return this.http.delete(this.empresaUrl + "/"+ id);
  }

}
