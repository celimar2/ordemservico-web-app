import {Injectable} from '@angular/core';
import {Globals} from "../../shared/globals";
import {HttpClient} from "@angular/common/http";
import {Estado} from "../models/estado.model";
import {Municipio} from "../models/municipio.model";

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(private http:HttpClient, private globals:Globals) {}

  private estadoUrl = this.globals.HOSTNAME_PORT + '/api/estados';


  public getList() {
    return this.http.get<Estado[]>(this.estadoUrl);
  }

  public getByUf(uf) {
    return this.http.get<Estado>(this.estadoUrl + "/"+uf);
  }

  public getMunicipioByUf(uf) {
    return this.http.get<Municipio[]>(this.estadoUrl + "/"+uf+'/municipios');
  }

  public update(estado) {
    return this.http.put(this.estadoUrl, estado);
  }

  public create(estado) {
    return this.http.post<Estado>(this.estadoUrl, estado);
  }

  public delete(uf) {
    return this.http.delete(this.estadoUrl + "/"+ uf);
  }

}
